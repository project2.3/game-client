package src.gameclient.resources;

import java.util.Map;

/**
 * Resource class
 * Manages all config files
 */
public class Resource extends StaticResources
{
    private String filename;

    /**
     * Resouce class constructor
     *
     * @param filename Resouce file name
     * @author Niek van der Velde
     */
    public Resource(String filename)
    {
        this.filename = filename;
    }
    
 
    /**
     * Get resource variable
     *
     * @param key Resource key
     * @return String
     */
    public String get(String key)
    {
        // Get resource file key value Map
        Map<String, String> values = super.getResources(filename);

        // Return value
        return values.get(key);
    }
}
