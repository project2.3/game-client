package src.gameclient.resources;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import src.gameclient.logger.Logger;

/**
 * StaticResources
 * Manages resource files
 */
public class StaticResources {
    private final String directory = "resources/";
    private final String fileExtention = ".resource";
    private static Map<String, Map<String, String>> resources = new HashMap<String, Map<String, String>>();
    private static StaticResources selfInstance;
    private Logger logger = new Logger("Resources");

    /**
     * Get self instance of StaticResources
     *
     * @return StaticResources
     * @author Niek van der Velde
     */
    private StaticResources getInstance() {
        // Check if instance is set
        if (selfInstance != null) {
            return selfInstance;
        } else {
            return new StaticResources();
        }
    }

    /**
     * Get static resources map
     *
     * @return Map<String, Map<String, String>>
     * @author Niek van der Velde
     */
    public Map<String, Map<String, String>> getResourcesMap() {
        return resources;
    }

    /**
     * Push resource values to static resources Map
     *
     * @param filename Resource filename
     * @param values Key value resources of file
     * @author Niek van der Velde
     */
    public void pushResourceValues(String filename, Map<String, String> values) {
        resources.put(filename, values);
    }

    /**
     * Get resource values
     *
     * @param filename Resource filename
     * @return Map<String, String>
     * @author Niek van der Velde
     */
    protected Map<String, String> getResources(String filename) {
        // Get instance
        StaticResources staticResources = getInstance();

        // Check if resources file is already loaded
        Map<String, String> resourceValues = staticResources.getResourcesMap().get(filename);
        if (resourceValues != null) {
            return resourceValues;
        } else {
            return loadResource(filename, staticResources);
        }
    }

    /**
     * Load resource file and read/return key value
     *
     * @param filename Resource filename
     * @param staticResources StaticResources class instance
     * @return Map of key value
     * @author Niek van der Velde
     */
    private Map<String, String> loadResource(String filename, StaticResources staticResources) {
        logger.info("Loading resource file, "+ directory.concat(filename).concat(fileExtention));

        // Create new ResourceValue array
        Map<String, String> values = new HashMap<String, String>();

        // Check if file exists
        try (FileReader resourceFile = new FileReader(directory.concat(filename).concat(fileExtention))) {

            // Setup buffered reader
            BufferedReader resouceReader = new BufferedReader(resourceFile);
            String line = resouceReader.readLine();


            // Loop trough lines
            while (line != null) {
                // Remove comment from line
                line = line.split("###")[0];
                line = line.trim();

                // Check if line is not empty
                if (line.indexOf("=") > -1) {
                    // Split key and value
                    String key = line.split("=")[0];
                    String value = line.split("=")[1];

                    // Add key value to Map
                    values.put(key, value);
                }

                // Read next line
                line = resouceReader.readLine();
            }
        } catch (IOException e) {
            logger.error("Could not open file, "+ directory.concat(filename).concat(fileExtention));
        }

        // Save complete key value Map to static resources
        staticResources.pushResourceValues(filename, values);

        // Return Map
        return values;
    }
}
