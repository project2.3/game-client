package src.gameclient;

import javafx.application.Application;
import src.gameclient.apiconnection.HostConnector;
import src.gameclient.gui.MainApplication;

import java.io.IOException;

public class Main {
    public static void main (String[] args) {
        try {
            new Thread(HostConnector.getInstance()).start();
            Application.launch(MainApplication.class);
        } catch (IOException e) {
        }

    }
}
