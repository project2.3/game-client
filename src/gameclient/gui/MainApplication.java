package src.gameclient.gui;

import src.gameclient.gui.views.ChallengePopUP;
import src.gameclient.gui.views.LoginController;
import src.gameclient.gui.views.MainController;
import src.gameclient.logger.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApplication extends Application{
    private Stage primaryStage;
    private BorderPane mainLayout;
    private Model model = new Model();
    private List<String> openSecondaryStages = new ArrayList<>();
    private Map<String, BorderPane> openSecondaryPanes = new HashMap<String, BorderPane>();
    private Logger logger = new Logger("GUI");
    private BorderPane currentPane;
    private ControllerInterface currentController;

    /**
     * Start application
     *
     * @param primaryStage Primary stage
     * @throws Exception When stage can not be started for some reason
     * @author Rob Kat
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Setup primary stage
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Project 2.3");

        // Load main controller
        logger.info("Loading MainController");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(new MainController().getView());

        mainLayout = loader.load();
        logger.info("Loaded MainController");

        // Setup scene
        Scene scene = new Scene(mainLayout);
        primaryStage.setScene(scene);

        // Setup main controller
        ControllerInterface mainController = loader.<ControllerInterface>getController();
        mainController.setModel(model);
        mainController.setApplication(this);
        mainController.init();

        new ChallengePopUP(model, this);

        // Show primary stage
        primaryStage.show();
        // Show Login Controller
        showGui(new LoginController());

        // Set on close event
        logger.info("Setup on close request");
        primaryStage.setOnCloseRequest(e -> primaryStageCloseEvent());
    }

    /**
     * Show gui
     *
     * @param controller Controller instance
     * @throws IOException When the view can not be loaded
     * @author Rob Kat
     */
    public void showGui(ControllerInterface controller) {
        // Check if there is a current controller active
        if (currentController != null) {
            currentController.destroy();
        }

        logger.info("Loading new controller, "+ controller.getClass().getName());
        // Load new pane
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(controller.getView());
        BorderPane pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            logger.error("Could not load view");
        }
        logger.info("Controller loaded, "+ controller.getClass().getName());

        // Set new pane
        mainLayout.setCenter(pane);

        // Pass trough Model and Application
        controller = loader.<ControllerInterface>getController();
        controller.setModel(model);
        controller.setApplication(this);
        controller.init();
        currentController = controller;

        currentPane = pane;

        // Set primaryStage title
        primaryStage.setTitle(controller.getTitle());
    }

    /**
     * Create new scene
     *
     * @param controller GUI Controller
     * @author Niek van der Velde
     */
    public BorderPane newStage (ControllerInterface controller) throws IOException {
        logger.info("Creating new stage");
        // Get controller name
        String controllerName = controller.getClass().getName();

        // Check if stage is not already open
        if (!openSecondaryStages.contains(controllerName)) {
            // Add controller to open stages
            openSecondaryStages.add(controllerName);

            // Create new stage
            Stage stage = new Stage();
            stage.setTitle(controller.getTitle());
            stage.setOnCloseRequest(e -> secondaryStageCloseEvent(controllerName));

            // Load controller pane
            logger.info("Loading new controller, "+ controller.getClass().getName());
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(controller.getView());
            BorderPane pane = loader.load();
            logger.info("Loaded new controller, "+ controller.getClass().getName());

            // Add pane to openSecondaryPanes
            openSecondaryPanes.put(controllerName, pane);

            // Set controller variables
            controller = loader.<ControllerInterface>getController();
            controller.setModel(model);
            controller.setApplication(this);
            controller.init();

            // Create new scene
            Scene scene = new Scene(pane);

            // Show stage
            stage.setScene(scene);
            stage.show();

            return pane;
        } else {
            return null;
        }
    }

    /**
     * Secondary stage close event
     *
     * @author Niek van der Velde
     */
    private void secondaryStageCloseEvent(String controllerName) {
        logger.info("Closing secondary stage, "+ controllerName);
        // Remove stage from open stages
        openSecondaryStages.remove(controllerName);
        openSecondaryPanes.remove(controllerName);
    }

    /**
     * PrimaryStage on close event
     *
     * @author Niek van der Velde
     */
    private void primaryStageCloseEvent() {
        logger.info("On close request triggert, closing application");
        // Close application
        Platform.exit();
        System.exit(0);
    }

    /**
     * Get secondary stage pane
     *
     * @param controller Controller
     * @return BorderPane
     * @author Niek van der Velde
     */
    public BorderPane getStagePane(ControllerInterface controller) {
        return openSecondaryPanes.get(controller.getClass().getName());
    }

    /**
     * NOTE: unused function
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
