package src.gameclient.gui;

import src.gameclient.apiconnection.GameAPI;
import src.gameclient.apiconnection.HostConnector;

import java.io.IOException;

public class Model {
    private String username;
    private boolean inGame = false;
    private AITypes ai;
    private String opponent;
    private Boolean yourTurn;
    private ControllerEnum controller;
    private GameTypeEnum gameType;

    public enum AITypes {
        RANDOM, CHALLENGER
    }

    public enum ControllerEnum {
        LOBBY, LOGIN, GAMEBOARD, MAINMENU
    }

    public enum GameTypeEnum {
        TICTACTOE, REVERSI
    }

    public void setAi(AITypes ai) {
        this.ai = ai;
    }

    public AITypes getAi() {
        return ai;
    }

    public void setGameType(GameTypeEnum gameType) {
        this.gameType = gameType;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setController(ControllerEnum controller) {
        this.controller = controller;
    }

    public void makeConnection() {
        GameAPI.setInstanceToNull();
        try {
            new Thread(HostConnector.getInstance()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getInGame() {
        return inGame;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }


    public ControllerEnum getController() {
        return controller;
    }

    public GameTypeEnum getGameType() {
        return gameType;
    }

    /**
     * Set opponent
     *
     * @param opponent Opponent
     * @author Niek van der Velde
     */
    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    /**
     * Get opponent
     *
     * @return String
     * @author Niek van der Velde
     */
    public String getOpponent() {
        return opponent;
    }

    public void setYourTurn(boolean yourTurn) {
        this.yourTurn = yourTurn;
    }
    public Boolean getYourTurn() {
        return yourTurn;
    }

    public String getGameTitle(){
        return gameType.name();
    }
}
