package src.gameclient.gui;

import java.net.URL;

public interface ControllerInterface {
    public void setModel(Model model);
    public URL getView();
    public void setApplication(MainApplication application);
    public String getTitle();
    public void init();
    public void destroy();
}
