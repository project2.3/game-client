package src.gameclient.gui.views;

import java.net.URL;
import java.io.IOException;
import java.util.Map;

import javafx.application.Platform;

import src.gameclient.gamemodes.gui.GameModeController;
import src.gameclient.apiconnection.HostConnector;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.apiconnection.ServerInterpreter;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;
import src.gameclient.logger.gui.LoggerController;
import src.gameclient.logger.Logger;

public class MainController implements ControllerInterface{
    private Model model;
    private MainApplication application;
    private Logger logger = new Logger("GUI");

    public void goHome() {
        if(model.getController() == Model.ControllerEnum.MAINMENU){
            GameAPI.getInstance().logout();
            model.makeConnection();
            application.showGui(new LoginController());
        } else if(model.getController() == Model.ControllerEnum.LOBBY){
            application.showGui(new MainMenuController());
        } else if (model.getController() == Model.ControllerEnum.GAMEBOARD) {
            model.setYourTurn(false);
            model.setOpponent("");
            model.setInGame(false);
            GameAPI.getInstance().resign();
            application.showGui(new LobbyController());
        }
    }

    /**
     * Open logger scene
     *
     * @author Niek van der Velde
     */
    public void openLogger() {
        try {
            application.newStage(new LoggerController());
        } catch (IOException e) {
            logger.error("Could not load LoggerController");
        }
    }

    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Override
    public URL getView() {
        return getClass().getResource("main.fxml");
    }

    @Override
    public void setApplication(MainApplication application) {
        this.application = application;

    }

    /**
     * MainController init
     *
     * @author Niek van der Velde
     */
    public void init() {
        // Set listener on start match
        try {
            HostConnector.getInstance().addObserver(new ServerInterpreter("game:match", e -> startMatch((Map<String, String>) e)));
        } catch (IOException e) {
            logger.error("Server error");
        }
    }

    /**
     * Start match
     *
     * @param match New match data
     * @author Niek van der Velde
     */
    private void startMatch(Map<String, String> match) {
        // Set match data to model
        model.setInGame(true);
        model.setGameType(matchGetGameType(match));
        model.setOpponent(match.get("OPPONENT"));
        model.setYourTurn(match.get("PLAYERTOMOVE").equals(model.getUsername()));

        // Show game mode controller
        Platform.runLater(() -> application.showGui(new GameModeController()));
    }

    private Model.GameTypeEnum matchGetGameType (Map<String, String> match){
        switch (match.get("GAMETYPE")){
            case "Reversi":
                return Model.GameTypeEnum.REVERSI;
            case "Tic-tac-toe":
                return Model.GameTypeEnum.TICTACTOE;
        }
        return null;
    }

    /**
     * Get stage title
     *
     * @return String
     * @author Niek van der Velde
     */
    public String getTitle() {
        return "Main";
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
    }
}
