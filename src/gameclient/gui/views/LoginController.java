package src.gameclient.gui.views;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.apiconnection.HostConnector;
import src.gameclient.apiconnection.ServerInterpreter;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class LoginController implements ControllerInterface {
    private Model model;
    private MainApplication application;
    private ServerInterpreter playerListInterpreter;

    @FXML private TextField textFieldUsername;
    @FXML private TextField textFieldFeedback;


    /**
     * When login is pressed checkLogin() will be executed and request from the API the players list. The given
     * username will be checked with playerInList.
     *
     * @throws IOException
     * @author Rob Kat
     */
    public void checkLogin() throws IOException {
        GameAPI.getInstance().getPlayers();
    }

    /***
     * When a name is typed in the text field and the name isn't empty playerInList will check all the players and
     * return 'Invalid username' when the given username is the same as a given username. To prevent errors on the
     * server side all the spaces will be trimmed.
     *
     * @param playerList
     * @throws IOException
     * @author Rob Kat
     */
    public void playerInList(ArrayList playerList) throws IOException{
        if(textFieldUsername != null) {
            if(!textFieldUsername.getText().isEmpty()) {
                if(playerList.size() !=0) {
                    for (int i = 0; i < playerList.size(); i++) {
                        if (textFieldUsername.getText().trim().equals(playerList.get(i))) {
                            textFieldFeedback.setText("Invalid username");
                            break;
                        } else if (i + 1 == playerList.size()) {
                            login();
                        }
                    }
                }else {
                    login();
                }
            }
        }
    }

    private void login(){

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GameAPI.getInstance().login(textFieldUsername.getText().trim());
                model.setUsername(textFieldUsername.getText().trim());
                application.showGui(new MainMenuController());
            }
        });

    }


    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Override
    public URL getView() {
        return getClass().getResource("login.fxml");
    }

    @Override
    public void setApplication(MainApplication application) {
        this.application = application;

    }

    /**
     * LoginController init
     *
     * @author Niek van der Velde
     */
    public void init() {
        try {
            playerListInterpreter = new ServerInterpreter("playerlist", e -> {
                try {
                    playerInList((ArrayList<String>)e);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            });

            HostConnector.getInstance().addObserver(playerListInterpreter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get stage title
     *
     * @return String
     * @author Niek van der Velde
     */
    public String getTitle() {
        return "Login";
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
        try {
            HostConnector.getInstance().deleteObserver(playerListInterpreter);
        } catch (IOException e) {
        }
    }
}
