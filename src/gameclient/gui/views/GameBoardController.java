package src.gameclient.gui.views;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.gamemodes.OnMoveCommand;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;
import src.gameclient.logger.Logger;

import java.net.URL;


public class GameBoardController implements ControllerInterface {
    private Model model;
    private MainApplication application;
    private Logger logger = new Logger("GUI");
    private OnMoveCommand onMove;
    @FXML private BorderPane mainBorderPane;

    /**
     * Init function of controller
     *
     * @author Niek van der Velde
     */
    @Override
    public void init() {
        model.setController(Model.ControllerEnum.GAMEBOARD);
    }

    /**
     * this createBoard creates a board with only one background color. This function just calls the other createBoard
     * function with twice the same color
     *
     * @param width
     * @param height
     * @param color
     * @param borderColor
     */
    public void createBoard(double width, double height, String color, String borderColor) {
        createBoard(width, height, color, color, borderColor);
    }

    /**
     * createBoard creates a board with the given width and height and the given colors.
     *
     * @author Rob Kat
     * @param width
     * @param height
     * @param firstColor
     * @param secondColor
     * @param borderColor
     */
    public void createBoard(double width, double height, String firstColor, String secondColor, String borderColor){
        StackPane stackPane = new StackPane();
        GridPane board = new GridPane();

        // Scalable settings for pane
        stackPane.setMinSize(width, height);
        stackPane.setMaxSize(width, height);
        stackPane.setStyle("-fx-background-color: " + borderColor);
        NumberBinding maxScale = Bindings.min(mainBorderPane.widthProperty().divide(width), mainBorderPane.heightProperty().divide(height));
        stackPane.scaleXProperty().bind(maxScale);
        stackPane.scaleYProperty().bind(maxScale);

        // Scalable settings for board
        board.setMinSize(width, height);
        board.setMaxSize(width, height);

        // Create 'width' amount of columns with all the same size
        for(int i = 0; i < width; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100 / width);
            board.getColumnConstraints().add(colConst);
        }

        // Create 'height' amount of rows with all the same size
        for(int i = 0; i < height; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100 / height);
            board.getRowConstraints().add(rowConst);
        }

        // Make board. Each cell is made up out of a pane with the right color and a transparent button in the size of the pane.
        String color = "firstColor";
        for(int i = 0; i < height; i++) {

            for(int j = 0; j < width; j++) {
                Pane innerPane = new Pane();
                Button button = new Button();
                button.setMaxSize(width, height);
                button.setMinSize(width, height);

                if(color.equals("firstColor")){
                    innerPane.setStyle("-fx-background-color: " + firstColor + "; -fx-border-color: " + borderColor + "; -fx-border-width: 0.02px;");
                    button.setStyle("-fx-background-radius: 0em; " + "-fx-min-width: 3px; " + "-fx-min-height: 3px; " + "-fx-padding: 0px;" + "-fx-border-color: black;" + "-fx-border-width: 0px;" + "-fx-background-color: transparent");
                    color = "secondColor";
                } else {
                    innerPane.setStyle("-fx-background-color: " + secondColor + "; -fx-border-color: " + borderColor + " ; -fx-border-width: 0.02px;");
                    button.setStyle("-fx-background-radius: 0em; " + "-fx-min-width: 3px; " + "-fx-min-height: 3px; " + "-fx-padding: 0px;" + "-fx-border-color: black;" + "-fx-border-width: 0px;" + "-fx-background-color: rgba(0,0,0,0)");
                    color = "firstColor";
                }
                button.setUserData((int) ((i * width) + j));
                button.setOnAction(this::makeMove);

                innerPane.getChildren().add(button);
                board.add(innerPane, j, i);
            }

            if(width % 2 == 0) {
                if(color.equals("firstColor")){
                    color = "secondColor";
                } else {
                    color = "firstColor";
                }

            }
        }

        stackPane.getChildren().add(board);
        mainBorderPane.setCenter(stackPane);
    }


    /**
     * setTile puts a game piece on the board or, when a tile is already set, changes the tile.
     * @param location
     */
    public void setTile(int location, GameModeInterface.Players player){
        GridPane boardGridPane = (GridPane)getBoardStackPane().getChildren().get(0); // Get gridPane where all subPanes(tiles) are made in
        Pane subBorderPane = (Pane) boardGridPane.getChildren().get(location); // Get borderPane where images have to be load into

        Image image = null;
        switch(player) {
            case USER:
                image = new Image("File:images/" + getTitle() + "/" + "you" + ".png");
            break;
            case OPPONENT:
                image = new Image("File:images/" + getTitle() + "/" + "they" + ".png");
            break;
        }

        // Load image to ImageView, bind width and height properties to image and put image in right location
        ImageView imageView = new ImageView(image);
        imageView.fitWidthProperty().bind(subBorderPane.widthProperty());
        imageView.fitHeightProperty().bind(subBorderPane.heightProperty());

        Button button;
        if(subBorderPane.getChildren().size() == 2){
            button = (Button) subBorderPane.getChildren().get(1);
        } else {
            button = (Button) subBorderPane.getChildren().get(0);
        }
        Platform.runLater( () -> {
            subBorderPane.getChildren().clear();

            subBorderPane.getChildren().addAll(imageView, button);
        });
    }

    public StackPane getBoardStackPane(){
        //get stackpane were game is made on
        return (StackPane) mainBorderPane.getCenter();
    }

    /**
     * When a tile is pressed execute the following code. The location of the pressed tile is saved in userData
     * @param event
     */
    public void makeMove(ActionEvent event) {
        Node node = (Node) event.getSource();
        // Check if on move is set
        if (onMove != null) {
            onMove.execute((int) node.getUserData());
        }
    }

    /**
     * Set on move command
     *
     * @param command On Move command
     * @author Niek van der Velde
     */
    public void onMoveCommand(OnMoveCommand command) {
        onMove = command;
    }

    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Override
    public URL getView() {
        return getClass().getResource("gameBoard.fxml");
    }

    @Override
    public void setApplication(MainApplication application) {
        this.application = application;
    }

    /**
     * Get title for primary stage
     *
     * @return String
     * @author Niek van der Velde
     */
    @Override
    public String getTitle() {
        return model.getGameTitle().toLowerCase();
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {}
}
