package src.gameclient.gui.views;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import src.gameclient.logger.Logger;
import src.gameclient.apiconnection.HostConnector;
import src.gameclient.apiconnection.ServerInterpreter;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;


public class ChallengePopUP {
    private Model model;
    private MainApplication application;
    private Logger logger = new Logger("GameMode");

    public ChallengePopUP(Model m, MainApplication application){
        this.model = m;
        this.application = application;

        // Add listener for new challenge
        try {
            HostConnector.getInstance().addObserver(new ServerInterpreter("game:challenge", e -> ShowPopUp((HashMap) e)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * creates a popup when challenged
     *
     * @param e challenge data
     */
    private void ShowPopUp(HashMap< String, String> e) {
        if (model.getAi() != null) {
            acceptChallenge(e);
        } else if (!model.getInGame() && e.get("CHALLENGER")!= null) {
            Platform.runLater( ()-> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Challenge");
                alert.setHeaderText(e.get("CHALLENGER") + " challenges you to a game off " + e.get("GAMETYPE"));
                alert.setContentText("Would you like to accept?");
                ButtonType accept = new ButtonType("Accept");
                alert.getButtonTypes().set(0, accept);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == accept) {
                    acceptChallenge(e);
                }

                alert.show();
                alert.hide();

            });
        }
    }

    /**
     * on click accept send accept to server
     * @param e
     */
    private void acceptChallenge(HashMap e) {
        logger.info("Challenge "+ e.get("CHALLENGENUMBER").toString() +" accepted");
        GameAPI.getInstance().acceptChallenge(
        Integer.parseInt(e.get("CHALLENGENUMBER").toString())); //accept challenge
    }
}
