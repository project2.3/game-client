package src.gameclient.gui.views;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;

import javafx.scene.control.Hyperlink;
import javafx.scene.layout.VBox;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.apiconnection.HostConnector;
import src.gameclient.apiconnection.ServerInterpreter;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class LobbyController implements ControllerInterface {
    private Model model;
    private MainApplication application;
    private String selectedPlayer;
    private ServerInterpreter playerListInterpreter;


    @FXML
    private VBox playerList;

    /**
     * Init function of controller
     *
     * @author Daan Adrichem
     * @author Niek van der Velde
     */
    @Override
    public void init() {
        model.setController(Model.ControllerEnum.LOBBY);
        try {
            playerListInterpreter = new ServerInterpreter("playerlist", e -> updatePlayerList((ArrayList) e));
            HostConnector.getInstance().addObserver(playerListInterpreter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * make Hyperlinks for each player
     *
     * @param e list of player names
     * @author Daan Adrichem
     */
    private void updatePlayerList(ArrayList<String> e) {

        for (String player: e) {
            if (!player.equals( model.getUsername() )){
                Hyperlink link = new Hyperlink();
                link.setText(player);
                link.setUserData(player);
                link.setOnAction(this::Player);
                addPlayers(link);
            }
        }
    }


    /**
     * set selectedPlayer as clicked Hyperlink userData
     *
     * @param actionEvent
     * @author Daan Adrichem
     */
    public void Player(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource() ;
        this.selectedPlayer = (String) node.getUserData();
    }

    /**
     * challenge the selectedPlayer  //on the current gameType
     *
     * NOTE: commented line needs to be added ones a game is passed
     * @param e
     * @author Daan Adrichem
     */
    public void challenge(ActionEvent e) {
        if(selectedPlayer != null) {
            //to be logged
            GameAPI.getInstance().challenge(selectedPlayer, model.getGameType());
            this.selectedPlayer=null;
        }
    }

    /**
     * add node to scrollable list
     *
     * @param node
     * @author Daan Adrichem
     */
    public void addPlayers (Node node){
        Platform.runLater(() -> {
          try {
              playerList.getChildren().add(node);
          }catch (Exception e){}

        });

    }

    /**
     * update the player list
     *
     * @param actionEvent
     * @author Daan Adrichem
     */
    public void update(ActionEvent actionEvent) {
        GameAPI.getInstance().getPlayers();
        playerList.getChildren().clear();
    }


    @Override
    public void setModel(Model m) {
        model = m; }

    @Override
    public URL getView() {
        return getClass().getResource("lobby.fxml");
    }

    @Override
    public void setApplication(MainApplication application) {
        this.application = application;
        GameAPI.getInstance().getPlayers(); //on initalization get all players
    }

    @Override
    public String getTitle() {
        return "Lobby";
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
        try {
            HostConnector.getInstance().deleteObserver(playerListInterpreter);
        } catch (IOException e) {
        }
    }
}
