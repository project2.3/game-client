package src.gameclient.gui.views;

import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.MainApplication;
import src.gameclient.gui.Model;

public class MainMenuController implements ControllerInterface {
    private Model model;
    private MainApplication application;

    @FXML private ComboBox gameModesComboBox;
    @FXML private ComboBox aiComboBox;

    /**
     * Init function of controller
     *
     * @author Niek van der Velde
     */
    @Override
    public void init() {
        // Set current controller
        model.setController(Model.ControllerEnum.MAINMENU);

        // Set available gamemodes
        ObservableList gamemodeItems = FXCollections.observableArrayList();
        for (Model.GameTypeEnum gamemode: Model.GameTypeEnum.values()) {
            String gamemodeName = gamemode.toString().toLowerCase();
            gamemodeName = gamemodeName.substring(0, 1).toUpperCase() + gamemodeName.substring(1);
            gamemodeItems.addAll(gamemodeName);
        }
        // Set items to gamemode ComboBox
        gameModesComboBox.setItems(gamemodeItems);

        // Set available ways to play
        ObservableList aiItems = FXCollections.observableArrayList();
        aiItems.addAll("Zelf spelen");

        // Loop trough AI enum
        for (Model.AITypes ai: Model.AITypes.values()) {
            String aiName = ai.toString().toLowerCase();
            aiName = aiName.substring(0, 1).toUpperCase() + aiName.substring(1);
            aiItems.addAll(aiName + " AI");
        }
        // Set items to AI ComboBox
        aiComboBox.setItems(aiItems);

        // Set active items to the first one
        gameModesComboBox.getSelectionModel().select(0);
        aiComboBox.getSelectionModel().select(0);
    }

    /**
     * On play button action
     *
     * @author Niek van der Velde
     */
    @FXML
    private void onPlay() {
        // Get selected items
        String gamemode = gameModesComboBox.getSelectionModel().getSelectedItem().toString();
        String ai = aiComboBox.getSelectionModel().getSelectedItem().toString();

        // Set AI to null
        model.setAi(null);

        // Loop trough gamemodes
        for (Model.GameTypeEnum gamemodeEnum : Model.GameTypeEnum.values()) {
            // Check if this type is the same
            if (gamemodeEnum.toString().toLowerCase().equals(gamemode.toLowerCase())) {
                model.setGameType(gamemodeEnum);
            }
        }

        // Loop trough ais
        for (Model.AITypes aiEnum : Model.AITypes.values()) {
            // Check if this type is the same
            if (aiEnum.toString().toLowerCase().equals(ai.toLowerCase().replace(" ai", ""))) {
                model.setAi(aiEnum);
            }
        }

        // Display lobby
        application.showGui(new LobbyController());
    }

    /**
     * Set model
     *
     * @param m Model
     * @author Niek van der Velde
     */
    @Override
    public void setModel(Model m) {
        model = m;
    }

    /**
     * Get view
     *
     * @return URL
     * @author Niek van der Velde
     */
    @Override
    public URL getView() {
        return getClass().getResource("mainMenu.fxml");
    }

    /**
     * Set application
     *
     * @param application MainApplication
     * @author Niek van der Velde
     */
    @Override
    public void setApplication(MainApplication application) {
        this.application = application;

    }

    /**
     * Get title for primary stage
     *
     * @return String
     * @author Niek van der Velde
     */
    @Override
    public String getTitle() {
        return "MainMenu";
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
    }
}
