package src.gameclient.logger.gui;

import java.net.URL;
import java.util.HashSet;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;

import src.gameclient.logger.StaticLogger;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.Model;
import src.gameclient.gui.MainApplication;

public class LoggerController implements ControllerInterface {
    // Class variables
    private Model model;
    private MainApplication application;
    private BorderPane pane;
    private HashSet<String> categories;
    private String currentCategory;
    @FXML
    private TabPane tabPane;
    @FXML
    private TextArea logTextArea;

    /**
     * Init function of controller
     *
     * @author Niek van der Velde
     */
    @Override
    public void init() {
        // Set listeners
        StaticLogger.listen((lt, c, t, m) -> newLog(lt, c, t, m));
        tabPane.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> tabStateChanged(ov, oldTab, newTab));

        // Get current categories
        categories = StaticLogger.getCategories();
        updateCategoryTabs(categories);
    }

    /**
     * Update category tabs
     *
     * @param categories HashSet of strings with category
     * @author Niek van der Velde
     */
    private void updateCategoryTabs(HashSet<String> categories) {
        // Clear current panes
        tabPane.getTabs().clear();

        // Loop trough set
        for (String category: categories) {
            // Create new tab
            Tab categoryTab = new Tab(category);
            categoryTab.setClosable(false);

            // Add tab to tabs pane
            tabPane.getTabs().add(categoryTab);
        }
    }

    /**
     * Tab state change
     *
     * @param event Change event
     * @param oldTab Old active tab
     * @param newTab New active tab
     * @author Niek van der Velde
     */
    private void tabStateChanged(Object event, Tab oldTab, Tab newTab) {
        // Set new current category
        currentCategory = newTab.getText();

        // Clear area
        logTextArea.setText("");

        // Add log content to TextArea
        for (String logLine: StaticLogger.readLogs(currentCategory)) {
            logTextArea.appendText(logLine + "\n");
        }
    }

    /**
     * New log from logger
     *
     * @param logType Log type
     * @param category Log category
     * @param timestamp Timestamp
     * @param message Message
     * @author Niek van der Velde
     */
    private void newLog(StaticLogger.LogTypes logType, String category, String timestamp, String message) {
        if (!categories.contains(category)) {
            // Update category tabs
            updateCategoryTabs(categories);
        }

        // Check if categories match
        if (category.equals(currentCategory)) {
            // Add log line
            logTextArea.appendText(logType.toString() +"\t("+ timestamp +")\t"+ message +"\n");
        }
    }

    /**
     * Set model
     *
     * @param model Model
     * @author Niek van der Velde
     */
    @Override
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * Get view url
     *
     * @return URL
     * @author Niek van der Velde
     */
    @Override
    public URL getView() {
        return getClass().getResource("logger.fxml");
    }

    /**
     * Set main application
     *
     * @param application MainApplication
     * @author Niek van der Velde
     */
    @Override
    public void setApplication(MainApplication application) {
        this.application = application;
    }

    /**
     * Get stage title
     *
     * @return String
     * @author Niek van der Velde
     */
    @Override
    public String getTitle() {
        return "Loging...";
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
    }
}
