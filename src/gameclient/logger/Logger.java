package src.gameclient.logger;

public class Logger extends StaticLogger {
    // Class vraibales
    private String category;

    /**
     * Logger constructor
     *
     * @param category Category string
     * @author Niek van der Velde
     */
    public Logger (String category) {
        this.category = category;
    }

    /**
     * Log info message
     *
     * @param message Log message
     * @author Niek van der Velde
     */
    public void info (String message) {
        writeToLogfile(StaticLogger.LogTypes.INFO, category, message);
    }

    /**
     * Log error message
     *
     * @param message Log message
     * @author Niek van der Velde
     */
    public void error (String message) {
        writeToLogfile(StaticLogger.LogTypes.ERROR, category, message);
    }

    /**
     * Log warning message
     *
     * @param message Log message
     * @author Niek van der Velde
     */
    public void warning (String message) {
        writeToLogfile(StaticLogger.LogTypes.WARNING, category, message);
    }
}
