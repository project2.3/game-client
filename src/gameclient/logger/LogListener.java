package src.gameclient.logger;

public interface LogListener {
    public void execute(StaticLogger.LogTypes logType, String category, String timestamp, String message);
}
