package src.gameclient.logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

public class StaticLogger {

    // Class variables
    private final String logExtention = ".log";
    private final String logLocation = "logs/";
    private static LogListener listener;
    private static StaticLogger staticLogger;
    private static Map<String, Path> logFiles = new HashMap<String, Path>();
    private static HashSet<String> categories = new HashSet<String>();
    public static enum LogTypes {
        INFO,
        WARNING,
        ERROR
    }

    /**
     * Set log listener
     *
     * @param logListener New log listener interface
     * @author Niek van der Velde
     */
    public static void listen (LogListener logListener) {
        listener = logListener;
    }

    /**
     * Write log to log file
     *
     * @param logType Log type
     * @param category Log category
     * @param message log message
     * @author Niek van der Velde
     */
    protected void writeToLogfile (LogTypes logType, String category, String message) {
        // Get posible log file
        Path logFile = logFiles.get(category.toLowerCase());

        // Put category in hash set
        categories.add(category);

        // Check if PrintWriter exists
        if (logFile == null) {
            // Create new PrintWriter
            logFile = getLogfile(category.toLowerCase());
            logFiles.put(category.toLowerCase(), logFile);
        }

        try {
            // Print line to file
            String timestamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
            String fullMessage = logType.toString() +"\t("+ timestamp +")\t"+ message +"\n";
            Files.write(logFile, fullMessage.getBytes(), StandardOpenOption.APPEND);

            // Execute listener callback
            if (listener != null) {
                listener.execute(logType, category, timestamp, message);
            }
        } catch (IOException e) {}
    }

    /**
     * Get new log file
     *
     * @param logFileName Log file name
     * @return Path
     */
    private Path getLogfile (String logFileName) {
        // Get file Path
        Path path = Paths.get(logLocation.concat(logFileName).concat(logExtention));

        try {
            // Delete is file exists
            Files.deleteIfExists(path);
            Files.createFile(path);
        } catch (IOException e) {
        }

        return path;
    }

    /**
     * Read logger history
     *
     * @param category log category
     * @return String
     * @author Niek van der Velde
     */
    public static List<String> readLogs (String category) {
        // Check if file is in path
        Path logPath = logFiles.get(category.toLowerCase());
        if (logPath != null) {
            try {
                // Read file lines
                return Files.readAllLines(logPath);
            } catch (IOException e) {
            }
        }
        return null;
    }

    /**
     * Get categories
     *
     * @return Map<String>
     * @author Niek van der Velde
     */
    public static HashSet<String> getCategories() {
        return categories;
    }
}
