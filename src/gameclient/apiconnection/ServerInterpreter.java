package src.gameclient.apiconnection;

import src.gameclient.logger.Logger;
import java.util.*;

public class ServerInterpreter implements Observer, Command {

    /**
     * blacklist of 1 responds items from api
     * can not respond to length 1 responses
     */
    private List blacklist = new ArrayList<String>(){{
        add("OK");
        add("ERR");
    }};

    private String[] commandParts;
    private Command lambda;
    private Logger logger = new Logger("APIConnection");

    /**
     * ServerInterpreter constructor
     *
     * @param key Subscription key
     * @param lambda Executing lambda
     */
    public ServerInterpreter (String key, Command lambda) {
        logger.info("New listener for, "+ key);

        this.commandParts = convertKeyToCommand(key);
        this.lambda = lambda;
    }

    /**
     * Convert input key to command parts
     *
     * @param key = Input of observer
     * @return String array
     * @author Niek van der Velde
     */
    private String[] convertKeyToCommand (String key) {
        return key.toUpperCase().split(":");
    }

    /**
     * Check is servercommand is same as interpreter command
     *
     * @param serverCommandParts Server command parts as string array
     * @return Boolean
     * @author Niek van der Velde
     */
    private Boolean checkCommand (String[] serverCommandParts) {
        // Set helper variable
        Boolean correctCommand = true;

        // Loop trough command parts
        for (int i = 0; i < commandParts.length; i++) {
            // Check if parts are the same
            correctCommand = correctCommand ? serverCommandParts[i].equals(commandParts[i]) || commandParts[i].equals("*") : false;
        }

        // Return correct command
        return correctCommand;
    }

    /**
     * Get server command parts
     *
     * @param serverCommand Server command as string
     * @return String array
     * @author Niek van der Velde
     */
    private String[] getServerCommandParts (String serverCommand) {
        return serverCommand.replace("SVR ", "").split(" ");
    }

    /**
     * On server command
     *
     * @param o Observer
     * @param arg Server command
     */
    @Override
    public synchronized void update (Observable o, Object arg) {
        // Set server response
        String serverCommand = (String) arg;

        // Check if not in blacklist
        if (!blacklist.contains(serverCommand.split(" "))) {
            // Check is command is for observer
            String[] serverCommandParts = getServerCommandParts(serverCommand);
            if (checkCommand(serverCommandParts)) {
                // Get data from server command
                String data = null;
                if (serverCommand.contains("[") ) {
                    data = "[" + serverCommand.split("\\[")[serverCommand.split("\\[").length - 1];
                }else if (serverCommand.contains("{")){
                    data = "{" + serverCommand.split("\\{")[serverCommand.split("\\{").length - 1];
                }

                // Execute
                lambda.execute(TransformData(data));
            }
        }
    }

    /**
     *
     * takes in string from api and transforms to workable data structures
     *
     * @param data the incoming data to be transformed
     * @return data in correct form (array,String)
     * @author Daan Adrichem
     */
    private Object TransformData (String data) {
        if (data.contains("[")) {
            List array = new ArrayList();
            String[] tmp = data.split("\"");
            for (int i = 1; i < tmp.length; i = i+ 2) {

                array.add(tmp[i]);
            }
            return array;
        } else if (data.contains("{")) {
            String[] values = data.split("\\{")[1].split("}")[0].split(",");
            Map<String, String> tmpmap = new HashMap<>();
            for (String keyvalue : values) {
                String key = keyvalue.split(":")[0].trim();
                String value = "" + keyvalue.split(":")[1].replace("\"", "").trim();

                tmpmap.put(key, value);
            }

            return tmpmap;
        }
        return data;
    }

    /**
     * Execute
     * @param data
     */
    @Override
    public void execute(Object data) {

    }
}
