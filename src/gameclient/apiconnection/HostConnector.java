package src.gameclient.apiconnection;

import src.gameclient.resources.Resource;
import src.gameclient.logger.Logger;

import java.io.*;
import java.net.*;
import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HostConnector extends Observable implements Runnable {

    private static HostConnector instance;

    private Socket socket;
    private Logger logger = new Logger("APIConnection");
    private ExecutorService threadPool = Executors.newFixedThreadPool(1);

    /**
     * HostConnector constructor
     *
     * @throws IOException Server connection failed
     * @author Daan Adrichem
     */
    protected HostConnector () throws IOException {
        Resource server = new Resource("server");
        socket = new Socket(server.get("ip"), Integer.parseInt(server.get("port")) );
    }

    /**
     * if no instance exists creates one else return it
     *
     * @return Hostconnector instance
     * @throws IOException if can't connect to specified server
     * @author Daan Adrichem
     */
    public static HostConnector getInstance () throws IOException {
        if (instance == null) {
            instance = new HostConnector();
        }
        return instance;
    }

    /**
     * send data to server
     *
     * @param command what to send to server
     * @author Daan Adrichem
     */
    public void toServer (String command) {
        DataOutputStream outToServer;
        try {
            logger.info("Writing command to server, "+ command);
            outToServer = new DataOutputStream(socket.getOutputStream());
            outToServer.writeBytes(command + " \n");

        } catch (IOException e) {
            logger.error("Could not fetch from server, toServer()");
        }
    }

    /**
     * disconnect from server
     *
     * @author Daan Adrichem
     */
    public void disconnect () {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * read out the socket connection with server
     * update observers when new data has come in
     *
     * @author Daan Adrichem
     */
    @Override
    public void run() {
        try {
            // Get stream
            InputStream message = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(message));
            String line;

            // Go trough receiving messages
            while ((line = bufferedReader.readLine()) != null) {
                // Notify observers in new thread
                String serverResponse = line;
                threadPool.execute(() -> {
                    logger.info("Response from server: "+ serverResponse);
                    setChanged();
                    notifyObservers(serverResponse);
                });
            }
        } catch (IOException e) {

        }
    }
}
