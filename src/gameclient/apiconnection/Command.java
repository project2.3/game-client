package src.gameclient.apiconnection;

public interface Command {
    void execute(Object data);
}
