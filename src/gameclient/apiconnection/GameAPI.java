package src.gameclient.apiconnection;

import java.io.IOException;

import src.gameclient.gui.Model;
import src.gameclient.logger.Logger;

public class GameAPI {

    private HostConnector hostConnector;
    private static GameAPI instance = null;
    private Logger logger = new Logger("APIConnection");

    public static void setInstanceToNull(){
        instance = null;
    }

    /**
     * connects to server
     *
     * @author Daan Adrichem
     */
    protected GameAPI () {
        try {
            this.hostConnector = HostConnector.getInstance();
            logger.info("Server connection created");
        } catch (IOException e) {
            logger.error("Could not create server connection");
        }
    }

    /**
     * gives an instance of this
     *
     * @return GameAPI
     * @author Daan Adrichem
     */
    public static synchronized GameAPI getInstance () {
        if (instance == null) {
            instance = new GameAPI();
        }
        return instance;
    }


    /**
     * log on to server as loginName
     *
     * @param loginName name to login as, should be checked if name isn't on the server already
     * @author Daan Adrichem
     */
    public void login (String loginName) {
        hostConnector.toServer("login " + loginName);
    }


    /**
     * logout from server, connection is still held
     *
     * @author Daan Adrichem
     */
    public void logout () {
        hostConnector.toServer("logout");
        
    }


    /**
     * get array of supported games
     *
     * @author Daan Adrichem
     */
    public void getGames () {
        hostConnector.toServer("get gamelist");
    }

    /**
     * get array of players currently on server
     *
     * @author Daan Adrichem
     */
    public void getPlayers () {
        hostConnector.toServer("get playerlist");
    }


    /**
     * subscribe to specific gametype
     *
     * @param gameType name of game to subscripe to, is capital sensitive. check if on server with getGames()
     * @author Daan Adrichem
     */
    public void subscripe (String gameType) {
        hostConnector.toServer("subscribe " + gameType);
    }

    /**
     * challenge player for gametype
     *
     * @param playerName player to challenge, is capital sensitive
     * @param gameType name of game to challenge player, is capital sensitive. check if on server with getGames()
     * @author Daan Adrichem
     */
    public void challenge (String playerName, Model.GameTypeEnum gameType) {

        switch(gameType){
            case REVERSI:
                hostConnector.toServer("challenge " + "\"" + playerName + "\"" + " \""+ "Reversi"+ "\"");
                break;
            case TICTACTOE:
                hostConnector.toServer("challenge " + "\"" + playerName + "\"" + " \""+ "Tic-tac-toe" + "\"");
                break;
        }
    }

    /**
     * accept challenge
     *
     * @param challengeNumber the number of the challenge to accept
     * @author Daan Adrichem
     */
    public void acceptChallenge (int challengeNumber) {
        hostConnector.toServer("challenge accept " + challengeNumber);
    }


    /**
     * make a move on the current active game
     *
     * @param move corresponding number for tile to make move on
     * @author Daan Adrichem
     */
    public void move (int move) {
        hostConnector.toServer("move " + move);
    }


    /**
     * resign from match
     * automatically lose match
     *
     * @author Daan Adrichem
     */
    public void resign () {
        hostConnector.toServer("forfeit");
    }

    /**
     * anny unsupported command to send to API
     *
     * @param command command to send to server
     */
    public void Commanding (String command) {
        hostConnector.toServer(command);
    }
}
