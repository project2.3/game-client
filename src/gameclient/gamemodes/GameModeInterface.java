package src.gameclient.gamemodes;

import javafx.scene.layout.BorderPane;
import src.gameclient.gui.views.GameBoardController;

public interface GameModeInterface {
    public BorderPane getBoardPane();
    public GameBoardController getBoardController();
    public void createBoard();
    public Boolean hasScore();
    public void setPlayerToMove(GameModeInterface.Players player);
    public void onBoardChange(OnBoardChangeCommand command);
    public void onNextTurn(OnNextTurnCommand command);
    public void onNewScore(OnNewScoreCommand command);
    public void onFinish(OnFinishCommand command);
    public Boolean makeMove(int tileLocation, Players player);
    public Players[] getGameState();
    public void setOpponentName(String opponent);

    public enum FinishStates {
        WON,
        LOST,
        DRAW,
        PLAYING
    }

    public enum Players {
        OPPONENT,
        USER
    }
}
