package src.gameclient.gamemodes.reversi;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.gamemodes.*;
import src.gameclient.gui.views.GameBoardController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.List;

public class Reversi implements GameModeInterface {

    public enum Direction {
        UP,
        LEFT,
        RIGHT,
        DOWN,
        DIAGONALLEFTUP,
        DIAGONALLEFTDOWN,
        DIAGONALRIGHTUP,
        DIAGONALRIGHTDOWN
    }

    private final int GAMESIZE = 64;
    private Players[]  gameState = new Players[GAMESIZE];

    private OnFinishCommand onFinishCommand;
    private OnNextTurnCommand onNextTurnCommand;
    private OnNewScoreCommand onNewScoreCommand;
    private OnBoardChangeCommand onBoardChangeCommand;

    private Players currentTurn;
    private String opponentName;

    private GameBoardController board;
    private BorderPane boardPane;

    /**
     * Reversi constructor
     *
     * @author Daan Adrichem
     */
    public Reversi() {
        setupBoard();
    }

    /**
     * update the score
     *
     * @author Daan Adrichem
     */
    private void setNewScore (){
        int playerScore = 0;
        int opponentScore = 0;
        for (Players tile: gameState){
            if (tile == Players.USER){
                playerScore++;
            }else if(tile == Players.OPPONENT){
                opponentScore++;
            }
        }
        onNewScoreCommand.execute(playerScore,opponentScore);
    }

    /**
     * set up the board with starting pieces
     *
     * @param beginner the player who makes the first move
     * @author Daan Adrichem
     */
    private void setUpStartingPositions(Players beginner) {
        ArrayList<Integer> changes = new ArrayList<>();

        gameState[27] = beginner == Players.USER ? Players.OPPONENT : Players.USER;
        gameState[36] = beginner == Players.USER ? Players.OPPONENT : Players.USER;
        changes.add(27);
        changes.add(36);
        onBoardChangeCommand.execute(changes, beginner == Players.USER ? Players.OPPONENT : Players.USER);

        changes.clear();

        gameState[28] = beginner == Players.OPPONENT ? Players.OPPONENT : Players.USER;
        gameState[35] = beginner == Players.OPPONENT ? Players.OPPONENT : Players.USER;
        changes.add(28);
        changes.add(35);

        onBoardChangeCommand.execute(changes, beginner);
    }

    @Override
    public Boolean makeMove(int tileLocation, Players player) {
        // Check if tile is null
        if (gameState[tileLocation] == null) {
            gameState[tileLocation] = player;
            changeAllCapturePieces(tileLocation, player);

            // Update game board with changes
            onBoardChangeCommand.execute(Arrays.asList(tileLocation), player);

            //set the score
            setNewScore();

            // Move is valid
            return true;

        }
        // Current move is not valid
        return false;
    }

    @Override
    public Players[] getGameState() {
        return gameState;
    }

    /**
     * check for each direction which stones are from the opponent and capture those
     *
     * @param location where the move is made
     * @param player the player which made the move
     * @author Daan Adrichem
     */
    public void changeAllCapturePieces(int location, Players player) {
        // Define changes array
        List<Integer> changes = new LinkedList<>();
        List<Integer> checkingHistory = new LinkedList<>();
        int direction = 0; // 0=topleft, 1=top, 2=topright, 3=right, 4=bottomright, 5=bottom, 6=bottomleft, 7=left
        int checkingTile = location;
        boolean done = false;

        // Loop trough all posible moves
        while (!done) {
            // Helper vars
            boolean directionDone = false;

            // Check if one of the four sides
            if ((checkingTile < 8 && (direction == 0 || direction == 1 || direction == 2)) ||
                (checkingTile > 55 && (direction == 4 || direction == 5 || direction == 6)) ||
                (checkingTile % 8 == 0 && (direction == 0 || direction == 7 || direction == 6)) ||
                ((checkingTile - 7) % 8 == 0 && (direction == 2 || direction == 3 || direction == 4))) {
                directionDone = true;
            } else {
                // Change current checking tile
                switch (direction) {
                    case 0:
                        checkingTile -= 9;
                        break;
                    case 1:
                        checkingTile -= 8;
                        break;
                    case 2:
                        checkingTile -= 7;
                        break;
                    case 3:
                        checkingTile += 1;
                        break;
                    case 4:
                        checkingTile += 9;
                        break;
                    case 5:
                        checkingTile += 8;
                        break;
                    case 6:
                        checkingTile += 7;
                        break;
                    case 7:
                        checkingTile -= 1;
                        break;
                }

                // Check if tile is outside board
                if (checkingTile > 63 || checkingTile < 0 || gameState[checkingTile] == null) {
                    directionDone = true;
                } else if (gameState[checkingTile] == player) { // Check if stone if player stone
                    // Legit set!!!!
                    directionDone = true;

                    // Loop trough changes
                    for (int changedTile : checkingHistory) {
                        gameState[changedTile] = player;
                    }

                    // Add all checked tiles to changes
                    changes.addAll(checkingHistory);
                } else {
                    checkingHistory.add(checkingTile);
                }
            }

            // Check if this direction is done
            if (directionDone) {
                checkingHistory = new LinkedList<>();
                checkingTile = location;
                direction++;

                // Check direction
                if (direction > 7) {
                    done = true;
                }
            }
        }

        // Update board with changes
        onBoardChangeCommand.execute(changes, player);
    }

    /**
     * for each edge check wether it is legal
     *
     * @param edges all edges
     * @param player the player whoms turn it is
     * @return ArrayList off legal moves
     * @author Daan Adrichem
     */
    private ArrayList<Integer> getLegalMoves(int[] edges, Players player) {
        ArrayList legalMoves = new ArrayList();
        for (int edge : edges){
            if (checkIfMoveLegal(edge, player)){
                legalMoves.add(edge);
            }
        }
        return legalMoves;
    }

    /**
     * check if the move is legal
     *
     * @param location which move to check
     * @param player who wants to make the move
     * @return bool if move is legal
     * @author Daan Adrichem
     */
    private boolean checkIfMoveLegal(int location, Players player) {
        for (Direction direction: Direction.values()) {
            int move = 0;
            int i = 1;
            switch (direction) {
                case UP:
                    move = -8;
                    break;
                case DOWN:
                    move = +8;
                    break;
                case LEFT:
                    move = -1;
                    break;
                case RIGHT:
                    move = +1;
                    break;
                case DIAGONALRIGHTDOWN:
                    move = +9;
                    break;
                case DIAGONALLEFTDOWN:
                    move = +7;
                    break;
                case DIAGONALLEFTUP:
                    move = -9;
                    break;
                case DIAGONALRIGHTUP:
                    move = -7;
                    break;
            }

            //loop through al directions
            while (location + (move * i) > 0 &&
                location + (move * i) <64 &&
                gameState[location + (move * i)] == GameModeInterface.Players.OPPONENT &&
                !(((location + (move * i)) %8 == 0) || ((location + (move * i)) % 8 == 7))){
                i++;
            }

            if (location + (move * i) > 0 && (location + (move * i) < 64)){
                if (gameState[location + (move * i)]==player && i > 1) { //check if move is legal on this direction
                    return true;
                }
            }
        }
        return false; //this move is not legal
    }


    private void setupBoard() {
        // Load board controller view
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(new GameBoardController().getView());
        try {
            boardPane = loader.load();
        } catch (IOException e) {

        }

        // Get controller
        board = loader.<GameBoardController>getController();
    }

    @Override
    public void createBoard() {
        board.init();
        board.createBoard(8, 8, "green", "black");

        setUpStartingPositions(currentTurn);
    }

    @Override
    public Boolean hasScore() {
        return true;
    }

    @Override
    public BorderPane getBoardPane() {
        return boardPane;
    }

    @Override
    public GameBoardController getBoardController() {
        return board;
    }

    @Override
    public void setPlayerToMove(Players player) {
        currentTurn = player;
    }

    @Override
    public void setOpponentName(String opponent) {
        this.opponentName = opponent;
    }

    @Override
    public void onNextTurn(OnNextTurnCommand command) {
        onNextTurnCommand = command;
    }

    @Override
    public void onNewScore(OnNewScoreCommand command) {
        onNewScoreCommand = command;
    }

    @Override
    public void onBoardChange(OnBoardChangeCommand command) {
        onBoardChangeCommand = command;
    }

    @Override
    public void onFinish(OnFinishCommand command) {
        onFinishCommand = command;
    }
}
