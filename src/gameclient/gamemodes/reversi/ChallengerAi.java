package src.gameclient.gamemodes.reversi;

import src.gameclient.gamemodes.GameAIInterface;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.logger.Logger;
import src.gameclient.resources.Resource;
import src.gameclient.gamemodes.OnMoveCommand;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.LinkedBlockingQueue;

public class ChallengerAi implements GameAIInterface {
    private Logger logger = new Logger("AI");
    private GameModeInterface.Players firstPlayer;
    private ThreadPoolExecutor threadPool;
    private Resource aiResource = new Resource("ai");
    private Node topNode;
    private OnMoveCommand makeMove;
    private Boolean running;
    private GameModeInterface.Players[] gameState;
    private int move;
    private GameModeInterface.Players moveMadeBy;

    public static int[] boardScores = new int[] {
        100, 4, 16, 12, 12, 16, 4, 100,
         4, 2, 6,   8,  8,  6, 2,  4,
        16, 6, 14, 10, 10, 14, 6, 16,
        12, 8, 10,  4,  4, 10, 8, 12,
        12, 8, 10,  4,  4, 10, 8, 12,
        16, 6, 14, 10, 10, 14, 6, 16,
         4, 2,  6,  8,  8,  6, 2,  4,
        100, 4, 16, 12, 12, 16, 4, 100
    };

    /**
     * ChallengerAi constructor
     *
     * @param player Player to move at begining of game
     * @author Rob Kat
     */
    public ChallengerAi(GameModeInterface.Players player) {
        firstPlayer = player;
        int threads = Integer.parseInt(aiResource.get("number_of_threads"));
        threadPool = new ThreadPoolExecutor(threads, threads, 1, TimeUnit.HOURS, new LinkedBlockingQueue<Runnable>());
        running = true;
    }

    /**
     * AI run
     *
     * @author Niek van der Velde
     */
    @Override
    public void run() {
        Date date1 = new Date();

        // Set new top node
        setNewTopNode(gameState, move, moveMadeBy);
        Date date2 = new Date();
        long diff = TimeUnit.MILLISECONDS.convert(date2.getTime() - date1.getTime(), TimeUnit.MILLISECONDS);

        // Sleep for amount of seconds
        try {
            Thread.sleep((Integer.parseInt(aiResource.get("time_to_think")) * 1000) - diff*2);
        } catch (Exception e) {
        }

        // Get best move from
        System.out.println("Best move: "+ topNode.getBestMove());
        
        makeMove.execute(topNode.getBestMove());
    }

    /**
     * Set make move command
     *
     * @param makeMove OnMoveCommand
     * @author Niek van der Velde
     */
    @Override
    public void setMakeMove(OnMoveCommand makeMove) {
        this.makeMove = makeMove;
    }

    /**
     * Update game state
     *
     * @param gameState New game state
     * @param move Tile that has been set
     * @param madeBy Player that changed the state
     * @author Niek van der Velde
     * @author Rob Kat
     */
    @Override
    public void setNewGameState(GameModeInterface.Players[] gameState, int move, GameModeInterface.Players madeBy) {
        // Set new game state
        this.gameState = gameState;
        this.move = move;
        this.moveMadeBy = madeBy;
    }

    /**
     * Set new top node and clear thread pool
     *
     * @param gameState New game state
     * @param move Int move
     * @param madeBy Player that changed the state
     * @author Niek van der Velde
     */
    private void setNewTopNode(GameModeInterface.Players[] gameState, int move, GameModeInterface.Players madeBy) {
        // Clear current queue for the threadpool
        int threads = Integer.parseInt(aiResource.get("number_of_threads"));
        threadPool.shutdownNow();
        threadPool = new ThreadPoolExecutor(threads, threads, 1, TimeUnit.HOURS, new LinkedBlockingQueue<Runnable>());

        // Check if we need to create a new node
        if (madeBy == GameModeInterface.Players.OPPONENT) {
            // Execute all leaves of new top node
            topNode = new Node(null, gameState, madeBy == GameModeInterface.Players.USER ? GameModeInterface.Players.OPPONENT : GameModeInterface.Players.USER, (childNode) -> {
                threadPool.execute(childNode);
            }, move);
            threadPool.execute(topNode);
        } else {
            topNode = new Node(gameState, madeBy, (childNode) -> {
                threadPool.execute(childNode);
            });
            threadPool.execute(topNode);
        }
    }

    /**
     * On game finished
     *
     * @author Niek van der Velde
     */
    @Override
    public void gameFinished() {
        // Stop thread pool and all tasks
        threadPool.shutdownNow();
        running = false;
    }
}
