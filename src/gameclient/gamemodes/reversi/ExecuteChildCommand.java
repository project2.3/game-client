package src.gameclient.gamemodes.reversi;

public interface ExecuteChildCommand {
    public void execute(Node childNode);
}
