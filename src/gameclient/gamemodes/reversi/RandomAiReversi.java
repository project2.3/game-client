package src.gameclient.gamemodes.reversi;

import src.gameclient.gamemodes.GameAIInterface;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.logger.Logger;
import src.gameclient.gamemodes.OnMoveCommand;

import java.util.ArrayList;
import java.util.Random;

public class RandomAiReversi implements GameAIInterface {

    private GameModeInterface.Players[] gameState;
    private Logger logger = new Logger("AI");
    private OnMoveCommand makeMove;
    private Boolean running = true;

    /**
     * On AI run
     *
     * @author Niek van der Velde
     */
    @Override
    public void run() {
        // Make best move
        ArrayList<Integer> legalMoves = getLegalMoves(getEdges(), GameModeInterface.Players.USER);
        makeMove.execute(legalMoves.get(new Random().nextInt(legalMoves.size())));
    }

    /**
     * Set make move command
     *
     * @param makeMove OnMoveCommand
     * @author Niek van der Velde
     */
    @Override
    public void setMakeMove(OnMoveCommand makeMove) {
        this.makeMove = makeMove;
    }

    @Override
    public void setNewGameState(GameModeInterface.Players[] gameState, int move, GameModeInterface.Players madeBy) {
        logger.info("New game state");
        this.gameState = gameState;
    }

    /**
     * for each edge check wether it is legal
     *
     * @param edges all edges
     * @param player the player whoms turn it is
     * @return ArrayList off legal moves
     * @author Daan Adrichem
     */
    private ArrayList<Integer> getLegalMoves(ArrayList<Integer> edges, GameModeInterface.Players player) {
        ArrayList legalMoves = new ArrayList();
        for (int edge : edges){
            if (checkIfMoveLegal(edge, player)){
                legalMoves.add(edge);
            }
        }
        return legalMoves;
    }


    /**
     * check if the move is legal
     *
     * @param location which move to check
     * @param player who wants to make the move
     * @return bool if move is legal
     * @author Daan Adrichem
     */

    private boolean checkIfMoveLegal(int location, GameModeInterface.Players player) {
        for (Reversi.Direction direction: Reversi.Direction.values()) {
            int move = 0;
            int i = 1;
            switch (direction) {
                case UP:
                    move = -8;
                    break;
                case DOWN:
                    move = +8;
                    break;
                case LEFT:
                    move = -1;
                    break;
                case RIGHT:
                    move = +1;
                    break;
                case DIAGONALRIGHTDOWN:
                    move = +9;
                    break;
                case DIAGONALLEFTDOWN:
                    move = +7;
                    break;
                case DIAGONALLEFTUP:
                    move = -9;
                    break;
                case DIAGONALRIGHTUP:
                    move = -7;
                    break;
            }

            //loop through al directions
            while (location + (move * i) > 0 &&
                location + (move * i) <64 &&
                gameState[location + (move * i)] == GameModeInterface.Players.OPPONENT &&
                !(((location + (move * i)) %8 == 0) || ((location + (move * i)) % 8 == 7))){
                i++;
            }

            if (location + (move * i) > 0 && (location + (move * i) < 64)){
                if (gameState[location + (move * i)]==player && i > 1) { //check if move is legal on this direction
                    return true;
                }
            }
        }
        return false; //this move is not legal

    }

    public ArrayList<Integer> getEdges() {
        ArrayList<Integer> edges = new ArrayList<>();

        int index = -1;

        for (GameModeInterface.Players tile: gameState) {
            index++;
            if(tile != null ){
                for (Reversi.Direction direction: Reversi.Direction.values()){

                    int move = 0;

                    switch (direction) {
                        case UP:
                            move = -8;
                            break;
                        case DOWN:
                            move = +8;
                            break;
                        case LEFT:
                            move = -1;
                            break;
                        case RIGHT:
                            move = +1;
                            break;
                        case DIAGONALRIGHTDOWN:
                            move = +9;
                            break;
                        case DIAGONALLEFTDOWN:
                            move = +7;
                            break;
                        case DIAGONALLEFTUP:
                            move = -9;
                            break;
                        case DIAGONALRIGHTUP:
                            move = -7;
                            break;
                    }

                    if (index + move >= 0 // Check if in board
                        && index + move < 64 // Check if in board
                        && gameState[index + move] == null) {
                        if (!edges.contains(index+move)) {
                            int edge = index+move;
                            edges.add(index + move);
                        }
                    }
                }
            }

        }
        return edges;
    }

    /**
     * On game finished
     *
     * @author Niek van der Velde
     */
    @Override
    public void gameFinished() {
        running = false;
    }
}
