package src.gameclient.gamemodes.reversi;

import src.gameclient.gamemodes.GameModeInterface;

import java.util.*;


public class Node implements Runnable {
    GameModeInterface.Players[] gameState;
    GameModeInterface.Players player;
    private Node parent;
    private int miniMaxScore;
    private ExecuteChildCommand executeChild;
    private int move;
    private int bestMove = -1;
    
    /**
     * makes the Mainnode with no parent
     *
     * @param gameState
     * @param currentTurn
     */
    public Node (GameModeInterface.Players[] gameState, GameModeInterface.Players currentTurn, ExecuteChildCommand executeChild) {
        this(null, gameState, currentTurn, executeChild, 0);
    }

    public Node (Node parent, GameModeInterface.Players[] gameState, GameModeInterface.Players currentTurn, ExecuteChildCommand executeChild, int move) {
        this.parent = parent;
        this.gameState = gameState;
        this.player = currentTurn;
        this.executeChild = executeChild;
        this.move = move;

        // Calculate score of current game state
        miniMaxScore = calculateScore();
        if (parent != null) {
            parent.updateScore(miniMaxScore, move);
        }
    }

    /**
     * check for each direction which stones are from the opponent and capture those
     *
     * @param location where the move is made
     * @param player the player which made the move
     * @author Daan Adrichem
     */
    public GameModeInterface.Players[] changeAllCapturePieces(GameModeInterface.Players[] gameState, int location, GameModeInterface.Players player){
        // Define Helper vars
        GameModeInterface.Players[] state = gameState;
        List<Integer> checkingHistory = new LinkedList<>();
        int direction = 0; // 0=topleft, 1=top, 2=topright, 3=right, 4=bottomright, 5=bottom, 6=bottomleft, 7=left
        int checkingTile = location;
        boolean done = false;

        // Loop trough all posible moves
        while (!done) {
            // Helper vars
            boolean directionDone = false;

            // Check if one of the four sides
            if ((checkingTile < 8 && (direction == 0 || direction == 1 || direction == 2)) ||
                (checkingTile > 55 && (direction == 4 || direction == 5 || direction == 6)) ||
                (checkingTile % 8 == 0 && (direction == 0 || direction == 7 || direction == 6)) ||
                ((checkingTile - 7) % 8 == 0 && (direction == 2 || direction == 3 || direction == 4))) {
                directionDone = true;
            } else {
                // Change current checking tile
                switch (direction) {
                    case 0:
                        checkingTile -= 9;
                        break;
                    case 1:
                        checkingTile -= 8;
                        break;
                    case 2:
                        checkingTile -= 7;
                        break;
                    case 3:
                        checkingTile += 1;
                        break;
                    case 4:
                        checkingTile += 9;
                        break;
                    case 5:
                        checkingTile += 8;
                        break;
                    case 6:
                        checkingTile += 7;
                        break;
                    case 7:
                        checkingTile -= 1;
                        break;
                }

                // Check if tile is outside board
                if (checkingTile > 63 || checkingTile < 0 || state[checkingTile] == null) {
                    directionDone = true;
                } else if (state[checkingTile] == player) { // Check if stone if player stone
                    // Legit set!!!!
                    directionDone = true;

                    // Loop trough changes
                    for (int changedTile : checkingHistory) {
                        state[changedTile] = player;
                    }
                } else {
                    checkingHistory.add(checkingTile);
                }
            }

            // Check if this direction is done
            if (directionDone) {
                checkingHistory = new LinkedList<>();
                checkingTile = location;
                direction++;

                // Check direction
                if (direction > 7) {
                    done = true;
                }
            }
        }

        return state;
    }


    /**
     * for each edge check whether it is legal
     *
     * @param edges all edges
     * @param player the player whoms turn it is
     * @return ArrayList off legal moves
     * @author Daan Adrichem
     */
    public ArrayList<Integer> getLegalMoves(ArrayList<Integer> edges, GameModeInterface.Players player) {
        ArrayList legalMoves = new ArrayList();
        for (int edge : edges){
            if (checkIfMoveLegal(edge, player)){
                legalMoves.add(edge);
            }
        }
        return legalMoves;
    }


    /**
     * check if the move is legal
     *
     * @param location which move to check
     * @param player who wants to make the move
     * @return bool if move is legal
     * @author Daan Adrichem
     */
    private boolean checkIfMoveLegal(int location, GameModeInterface.Players player) {
        for (Reversi.Direction direction: Reversi.Direction.values()) {
            int move = 0;
            int i = 1;
            switch (direction) {
                case UP:
                    move = -8;
                    break;
                case DOWN:
                    move = +8;
                    break;
                case LEFT:
                    move = -1;
                    break;
                case RIGHT:
                    move = +1;
                    break;
                case DIAGONALRIGHTDOWN:
                    move = +9;
                    break;
                case DIAGONALLEFTDOWN:
                    move = +7;
                    break;
                case DIAGONALLEFTUP:
                    move = -9;
                    break;
                case DIAGONALRIGHTUP:
                    move = -7;
                    break;
            }

            //loop through al directions
            while (location + (move * i) >= 0 &&
                location + (move * i) < 64 &&
                gameState[location + (move * i)] == (player == GameModeInterface.Players.USER ? GameModeInterface.Players.OPPONENT : GameModeInterface.Players.USER) &&
                !(((location + (move * i)) % 8 == 0) || ((location + (move * i)) % 8 == 7))){
                i++;
            }

            if (location + (move * i) >= 0 && (location + (move * i) < 64)){
                if (gameState[location + (move * i)] == player && i > 1) { //check if move is legal on this direction
                    return true;
                }
            }
        }
        return false; //this move is not legal
    }

    private ArrayList<Integer> getEdges(){
        ArrayList<Integer> edges = new ArrayList<>();

        int index = -1;

        for (GameModeInterface.Players tile: gameState) {
            index++;
            if(tile != null ){
                for (Reversi.Direction direction: Reversi.Direction.values()){

                    int move = 0;

                    switch (direction) {
                        case UP:
                            move = -8;
                            break;
                        case DOWN:
                            move = +8;
                            break;
                        case LEFT:
                            move = -1;
                            break;
                        case RIGHT:
                            move = +1;
                            break;
                        case DIAGONALRIGHTDOWN:
                            move = +9;
                            break;
                        case DIAGONALLEFTDOWN:
                            move = +7;
                            break;
                        case DIAGONALLEFTUP:
                            move = -9;
                            break;
                        case DIAGONALRIGHTUP:
                            move = -7;
                            break;
                    }

                    if (index + move >= 0 // Check if in board
                        && index + move < 64 // Check if in board
                        && gameState[index + move] == null) {
                        if (!edges.contains(index+move)) {
                            edges.add(index + move);
                        }
                    }

                }
            }

        }
        return edges;
    }

    public GameModeInterface.Players[] changeState(int move) {
        GameModeInterface.Players[] state = gameState.clone();
        state[move] = player;
        state = changeAllCapturePieces(state, move, player);
        return state;
    }

    @Override
    public void run() {
        ArrayList<Integer> legalMoves = getLegalMoves(getEdges(), player);

        for (int legalMove : legalMoves) {
            Node childNode = new Node(this, changeState(legalMove), player == GameModeInterface.Players.USER ? GameModeInterface.Players.OPPONENT : GameModeInterface.Players.USER, executeChild, legalMove);
            executeChild.execute(childNode);
        }

        // Reset vars to save memory
        gameState = null;
    }

    /**
     * Calculate current gamestate score
     *
     * @return int
     * @author Niek van der Velde
     */
    private int calculateScore() {
    
        int extraMultiplier = 1;
        int score = 1;
        int opponent = 0;
        int user = 0;
        boolean containsOPPONENT = false;
        
        if (getLegalMoves(getEdges(), player).size() == 0 &&
            getLegalMoves(getEdges(),player == GameModeInterface.Players.USER?
                GameModeInterface.Players.OPPONENT: GameModeInterface.Players.USER).size() == 0) {
            // Loop trough stones
            for (GameModeInterface.Players tile : gameState) {
                if (tile != null) {
                    if (!tile.equals(player)) {
                        opponent += 1;
                    } else if (tile.equals(player)) {
                        user += 1;
                    }
                }
            }
            if (user > opponent) {
                score = 1000000000;
            } else if (opponent < user) {
                score = -1000000000;
            } else if(opponent == user) {
                score = 0;
                //hier is het een draw wat voor score moeten we daar aan geven?
            }
        }else {
            //add score
            for (int i = 0; i < gameState.length; i++) {
        
                //rob zijn stukje
                if (move - 8 == i || move + 8 == i ||
                        move - 1 == i || move + 1 == i ||
                        move - 9 == i || move + 9 == i ||
                        move - 7 == i || move + 7 == i) {
            
                    if (checkIfMoveLegal(i, player == GameModeInterface.Players.USER ? GameModeInterface.Players.OPPONENT : GameModeInterface.Players.USER)) {
                        extraMultiplier++;
                    }
                }
                if (extraMultiplier <= 2) {
                    extraMultiplier = 2;
                } else {
                    extraMultiplier = 1;
                }
        
                //add the score
                score += ChallengerAi.boardScores[i];
        
                if (checkIfStableDisc(i, player)) {
                    score += 100;
                }/**/
        
                if (gameState[i] == player) {
                    score += ChallengerAi.boardScores[i];
                }
        
            }
            score += getLegalMoves(getEdges(), player).size() * 2.2;
    
    
        }
        score *=  extraMultiplier;
        
        if (!containsOPPONENT){
            score *= 1000;
        }
        // Return full score
        return score;
    }
    
    private boolean checkIfStableDisc(int i, GameModeInterface.Players player) {
        int[] directions = {1,7,8,9};
        for (int direction: directions) {
            for (int j = 0; i + (direction * j) < 64 && i - (direction * j) >= 0; j++){
                boolean captureOpportunities1 = false;
                boolean captureOpportunities2 = false;
                if (i + (direction * j) % 8 != 0) {
                    if ((gameState[i + (direction * j)] == null || gameState[i + (direction * j)] != player) && !captureOpportunities1) {
                        captureOpportunities1 = true;
                    }
                }
    
                if (i + (direction * j) % 8 != 7) {
                    if ((gameState[i - (direction * j)] == null || gameState[i - (direction * j)] != player && !captureOpportunities2)) {
                        captureOpportunities2 = true;
                    }
                    if (captureOpportunities1 &&
                            captureOpportunities2) {
                        return false;
                    }
                }
                
            }
            
        }
        return true;
    }
    
    /**
     * Update mini max score of current node
     *
     * @param childScore Int of child mini max score
     * @author Niek van der Velde
     */
    public void updateScore(int childScore, int move) {
        // Check player
        if (player == GameModeInterface.Players.USER && miniMaxScore <= childScore) {
            // Update score
            miniMaxScore = childScore;
            bestMove = move;
            if (parent != null) {
                parent.updateScore(miniMaxScore, this.move);
            } else {
//                System.out.println("New best move: "+ bestMove);
            }
        } else if (player == GameModeInterface.Players.OPPONENT && miniMaxScore >= childScore) {
            // Update score
            miniMaxScore = childScore;
            bestMove = move;
            if (parent != null) {
                parent.updateScore(miniMaxScore, this.move);
            } else {
//                System.out.println("New best move: "+ bestMove);
            }
        }

        if (bestMove == -1) {
            miniMaxScore = childScore;
            bestMove = move;
            
            
        }
    }

    /**
     * Get best move form mini max score
     *
     * @return int
     * @author Niek van der Velde
     */
    public int getBestMove() {
        if (miniMaxScore > 999999){
            System.out.println("winning");
        }else if (miniMaxScore < -999999){
            System.out.println("lossing");
        }
    
        if (bestMove == -1){
            ArrayList<Integer> legalMoves = getLegalMoves(getEdges(), GameModeInterface.Players.USER);
            bestMove = (legalMoves.get(new Random().nextInt(legalMoves.size())));
        }
        
        return bestMove;
    }

    /**
     * Get move from Node
     *
     * @return int
     * @author Niek van der Velde
     */
    public int getMove() {
        return move;
    }
}
