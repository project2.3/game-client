package src.gameclient.gamemodes.gui;

import java.net.URL;
import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;
import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import src.gameclient.apiconnection.HostConnector;
import src.gameclient.apiconnection.ServerInterpreter;
import src.gameclient.apiconnection.GameAPI;

import src.gameclient.logger.Logger;
import src.gameclient.gamemodes.GameAIInterface;
import src.gameclient.gamemodes.tictactoe.ChallengerAi;
import src.gameclient.gui.views.GameBoardController;
import src.gameclient.gamemodes.tictactoe.TicTacToe;
import src.gameclient.gamemodes.reversi.Reversi;
import src.gameclient.gamemodes.tictactoe.RandomAI;
import src.gameclient.gamemodes.reversi.RandomAiReversi;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.gui.ControllerInterface;
import src.gameclient.gui.Model;
import src.gameclient.gui.MainApplication;

public class GameModeController implements ControllerInterface {
    private Model model;
    private MainApplication application;
    private GameModeInterface gameMode;
    private Logger logger = new Logger("GameMode");
    private GameAIInterface gameAI;
    @FXML private BorderPane gameModeBorderPane;
    @FXML private Label opponentLabel;
    @FXML private Label currentTurnLabel;
    @FXML private Label scoreLabel;
    private GameBoardController board;
    private List<ServerInterpreter> interpreters = new LinkedList<ServerInterpreter>();

    /**
     * Game mode controller init function
     *
     * @author Niek van der Velde
     */
    @Override
    public void init() {
        // Set api observers
        setApiObservers();
        
        // Set current controller
        model.setController(Model.ControllerEnum.GAMEBOARD);

        // Set game and AI
        setGame();

        // Set game mode variables
        gameMode.setOpponentName(model.getOpponent());
        gameMode.setPlayerToMove(model.getYourTurn() ? GameModeInterface.Players.USER : GameModeInterface.Players.OPPONENT);
        
        // Create game board
        createGameBoard();
        
        // Initialize labels
        setOpponentLabel();
        setCurrentTurnLabel();
        scoreLabel.setVisible(gameMode.hasScore());

        // Make initial ai move
        initialAIMove();
    }

    /**
     * Set game and AI
     *
     * @author Niek van der Velde
     */
    private void setGame() {
        // Check game mode
        switch(model.getGameType()) {
            case TICTACTOE:
                gameMode = new TicTacToe();

                // Check AI
                if (model.getAi() != null) {
                    switch (model.getAi()) {
                        case RANDOM:
                            gameAI = new RandomAI();
                            break;
                        case CHALLENGER:
                            gameAI = new src.gameclient.gamemodes.tictactoe.ChallengerAi();
                            break;
                    }
                }
                break;
            case REVERSI :
                gameMode = new Reversi();

                // Check AI
                if (model.getAi() != null) {
                    switch (model.getAi()) {
                        case RANDOM:
                            gameAI = new RandomAiReversi();
                            break;
                        case CHALLENGER:
                            gameAI = new src.gameclient.gamemodes.reversi.ChallengerAi(model.getYourTurn() ? GameModeInterface.Players.USER : GameModeInterface.Players.OPPONENT);
                            break;
                    }
                }
                break;
        }

        // Log game and AI
        logger.info("Playing "+ model.getGameType());
        logger.info("AI mode: "+ model.getAi());
    }

    /**
     * Create game board
     *
     * @author Niek van der Velde
     */
    private void createGameBoard() {
        // Get board
        board = gameMode.getBoardController();
        board.setApplication(application);
        board.setModel(model);

        // Set listeners
        setListeners();

        // Get and set pane
        BorderPane boardPane = gameMode.getBoardPane();
        gameModeBorderPane.setCenter(boardPane);

        // Create board
        gameMode.createBoard();
    }

    /**
     * Set GameMode listeners
     *
     * @author Niek van der Velde
     */
    private void setListeners() {
        gameMode.onNextTurn((player) -> oponnentsTurn(player));
        gameMode.onNewScore((userScore, opponentScore) -> updateScore(userScore, opponentScore));
        gameMode.onFinish((result, reason) -> finishGame(result, reason));
        gameMode.onBoardChange((changes, player) -> updateBoard(changes, player));

        // Check if user or AI is playing
        if (model.getAi() == null) {
            board.onMoveCommand((tileLocation) -> makeMove(tileLocation));
        } else {
            gameAI.setMakeMove((tileLocation) -> makeMove(tileLocation));
        }
    }

    /**
     * Set api observers
     *
     * @author Niek van der Velde
     */
    private void setApiObservers() {
        // Remove current observers
        removeApiObservers();

        try {
            // Create interpreters
            interpreters.add(new ServerInterpreter("game:move", e -> onApiMove((HashMap<String, String>) e)));
            interpreters.add(new ServerInterpreter("game:yourturn", e -> onUserTurn()));
            interpreters.add(new ServerInterpreter("game:match", e -> onNewMatch()));
            interpreters.add(new ServerInterpreter("game:loss", e -> onApiFinish(GameModeInterface.FinishStates.LOST, (HashMap<String, String>) e)));
            interpreters.add(new ServerInterpreter("game:win", e -> onApiFinish(GameModeInterface.FinishStates.WON, (HashMap<String, String>) e)));
            interpreters.add(new ServerInterpreter("game:draw", e -> onApiFinish(GameModeInterface.FinishStates.DRAW, (HashMap<String, String>) e)));

            // Loop trough interpreters
            for (ServerInterpreter interpreter : interpreters) {
                HostConnector.getInstance().addObserver(interpreter);
            }
        } catch (IOException e) {
        }
    }

    /**
     * Remove all API observers
     *
     * @author Niek van der Velde
     */
    private void removeApiObservers() {
        try {
            // Loop trough interpreters
            for (ServerInterpreter interpreter : interpreters) {
                HostConnector.getInstance().deleteObserver(interpreter);
            }
        } catch (IOException e) {
        }
    }

    /**
     * On new match
     *
     * @author Niek van der Velde
     */
    private void onNewMatch() {
        // Set game and AI
        setGame();

        // Initialize labels
        setOpponentLabel();
        setCurrentTurnLabel();
        scoreLabel.setVisible(gameMode.hasScore());

        // Set game mode variables
        gameMode.setOpponentName(model.getOpponent());
        gameMode.setPlayerToMove(model.getYourTurn() ? GameModeInterface.Players.USER : GameModeInterface.Players.OPPONENT);

        // Set listeners
        setListeners();
    }

    /**
     * On users turn
     *
     * @author Niek van der Velde
     */
    private void onUserTurn() {
        // Set user turn
        model.setYourTurn(true);
        setCurrentTurnLabel();

        // Check if AI and if he needs to make a move
        if (model.getAi() != null) {
            new Thread(gameAI).start();
        }
    }

    /**
     * On api move event
     *
     * @param event Event information
     * @author Niek van der Velde
     */
    private void onApiMove(HashMap<String, String> event) {
        // Check if move is from user or opponent
        GameModeInterface.Players moveBy = event.get("PLAYER").equals(model.getUsername()) ? GameModeInterface.Players.USER : GameModeInterface.Players.OPPONENT;

        // Check for illegal move message from server
        if (!event.get("DETAILS").equals("Illegal move")) {
            // Check player
            gameMode.makeMove(Integer.parseInt(event.get("MOVE")), moveBy);
        } else {
            // Check wich player did the illegal move
            if (moveBy == GameModeInterface.Players.USER) {
                finishGame(GameModeInterface.FinishStates.LOST, event.get("DETAILS"));
            } else {
                finishGame(GameModeInterface.FinishStates.WON, event.get("DETAILS"));
            }
        }
    }

    /**
     * On api finish event
     *
     * @param result Result as FinishStates enum
     * @param event Hashmap of finish information
     * @author Niek van der Velde
     */
    private void onApiFinish(GameModeInterface.FinishStates result, HashMap<String, String> event) {
        finishGame(result, event.get("COMMENT"));
    }

    /**
     * Initial ai move
     *
     * @author Niek van der Velde
     */
    private void initialAIMove() {
        // Check if AI
        if (model.getAi() != null) {
            gameAI.setNewGameState(gameMode.getGameState(), 0, GameModeInterface.Players.USER);

            // Check if the AI needs to make a move
            if (model.getYourTurn()) {
                new Thread(gameAI).start();
            }
        }
    }

    /**
     * On make move event from BoardController
     *
     * @param tileLocation Title location that user selected
     * @author Niek van der Velde
     */
    private void makeMove(int tileLocation){
        if(model.getYourTurn()) {
            GameAPI.getInstance().move(tileLocation);
            oponnentsTurn(GameModeInterface.Players.OPPONENT);
            // gameMode.makeMove(tileLocation, GameModeInterface.Players.USER);
        }
    }

    /**
     * Update board
     *
     * @param changes Changes that have been made to the board
     * @param player Player that made the changes
     * @author Niek van der Velde
     */
    private void updateBoard(List<Integer> changes, GameModeInterface.Players player) {
        // Loop through board changes
        for (int tile: changes) {
            board.setTile(tile, player);
        }

        // Check for AI
        if (gameAI != null && changes.size() > 0) {
            gameAI.setNewGameState(gameMode.getGameState(), changes.get(0), player);
        }
    }

    /**
     * Next trun
     *
     * @param player String player name
     * @author Niek van der Velde
     */
    private void oponnentsTurn(GameModeInterface.Players player) {
        model.setYourTurn(false);
        setCurrentTurnLabel();
    }

    /**
     * On game finish
     * Update score
     *
     * @param userScore Int of user score
     * @param opponentScore Int of opponent score
     * @author Niek van der Velde
     */
    private void updateScore(int userScore, int opponentScore) {
        Platform.runLater(() -> scoreLabel.setText("You: "+ userScore + " , Opponent: "+opponentScore));
    }

    /**
     *
     * @param result
     * @param reason
     * @author Daan Adrichem, Niek van der Velde
     */
    private void finishGame(GameModeInterface.FinishStates result, String reason) {
        if (gameAI != null) {
            // Stop AI
            gameAI.gameFinished();
        }

        //create opaque result with result, option to look at the board or go back to lobby
        model.setInGame(false);
        Pane resultPane = new Pane();
        Label resultLabel;
        if (reason != null) {
            resultLabel = new Label(result.toString().concat(" : " + reason));
        } else {
            resultLabel = new Label(result.toString());
        }

        resultLabel.setStyle("-fx-font-size: 20px; -fx-min-height: 20px; -fx-text-fill: white;");
        resultLabel.prefHeightProperty().bind(resultPane.prefHeightProperty());
        resultPane.prefHeightProperty().bind(gameModeBorderPane.heightProperty().divide(8));
        resultPane.prefWidthProperty().bind(gameModeBorderPane.widthProperty());
        switch (result){
            case WON:
                resultPane.setStyle("-fx-background-color: green; -fx-min-height: 20px;");
                break;
            case LOST:
                resultPane.setStyle("-fx-background-color: red; -fx-min-height: 20px;");
                break;
            case DRAW:
                resultPane.setStyle("-fx-background-color: blue; -fx-min-height: 20px;");
                break;
        }
        resultPane.getChildren().add(resultLabel);

        Platform.runLater(() -> {
            resultLabel.layoutXProperty().bind(resultPane.widthProperty().subtract(resultLabel.widthProperty()).divide(2));
            Platform.runLater(() -> gameModeBorderPane.setBottom(resultPane));
        });
    }

    /**
     * Set current turn label text
     *
     * @author Daan Adrichem
     */
    public void setCurrentTurnLabel (){
        // Set username to label
        Platform.runLater(() -> {
            // Get current player that needs to make a move
            String username;

            if (model.getYourTurn()) {
                username = model.getUsername();
            } else {
                username = model.getOpponent();
            }

            currentTurnLabel.setText("Turn: ".concat(username));
        });
    }

    /**
     * Set score label text
     *
     * @param yourScore User score
     * @param opponentScore Opponent score
     * @author Daan Adrichem
     */
    public void setScoreLabel(int yourScore, int opponentScore) {
        Platform.runLater(()->scoreLabel.setText("You: " +yourScore + ", Opponent: " + opponentScore) );
    }

    /**
     * Set opponent label text
     *
     * @author Niek van der Velde
     */
    public void setOpponentLabel() {
        // Set label text
        Platform.runLater(() -> opponentLabel.setText("You're batteling against: ".concat(model.getOpponent())) );
    }

    /**
     * Get game mode view
     *
     * @return URL
     * @author Niek van der Velde
     */
    @Override
    public URL getView() {
        return getClass().getResource("gameMode.fxml");
    }

    /**
     * Set model
     *
     * @param model Model
     * @author Niek van der Velde
     */
    @Override
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * Set application
     *
     * @param application Application
     * @author Niek van der Velde
     */
    @Override
    public void setApplication(MainApplication application) {
        this.application = application;
    }

    /**
     * Get controller title
     *
     * @return String
     * @author Niek van der Velde
     */
    @Override
    public String getTitle() {
        return model.getGameTitle();
    }

    /**
     * On destroy
     *
     * @author Niek van der Velde
     */
    @Override
    public void destroy() {
        removeApiObservers();
    }
}
