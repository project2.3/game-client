package src.gameclient.gamemodes;

public interface OnNextTurnCommand {
    public void execute(GameModeInterface.Players player);
}
