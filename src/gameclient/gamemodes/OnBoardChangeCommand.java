package src.gameclient.gamemodes;

import java.util.List;

public interface OnBoardChangeCommand {
    public void execute(List<Integer> changes, GameModeInterface.Players player);
}
