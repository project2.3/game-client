package src.gameclient.gamemodes;

import src.gameclient.gamemodes.GameModeInterface;

public interface GameAIInterface extends Runnable {
    public void setNewGameState(GameModeInterface.Players[] gameState, int move, GameModeInterface.Players madeBy);
    public void setMakeMove(OnMoveCommand makeMove);
    public void gameFinished();
}
