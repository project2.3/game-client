package src.gameclient.gamemodes;

public interface OnMoveCommand {
    public void execute(int tileLocation);
}
