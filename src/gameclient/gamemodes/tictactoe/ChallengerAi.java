package src.gameclient.gamemodes.tictactoe;

import src.gameclient.gamemodes.GameAIInterface;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.gamemodes.OnMoveCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChallengerAi implements GameAIInterface {
    private GameModeInterface.Players[] boardState;
    private GameModeInterface.Players player;
    private GameModeInterface.FinishStates gameState = GameModeInterface.FinishStates.PLAYING;
    private Node mainNode;
    private OnMoveCommand makeMove;
    private Boolean running;

    /**
     * On ai thread run
     *
     * @author Niek van der Velde
     */
    public void run() {
        // Create the mainNode with the current boardState
        mainNode = new Node(boardState);

        // Create tree and save all the values
        mainNode.setScore(createTree(mainNode, GameModeInterface.Players.USER));

        // Start values for bestMove and bestScore.
        int bestMove = 0;
        int bestScore = -100;

        // Get all children from mainNode and save them in a List of nodes
        List<Node> listOfChildren = mainNode.getChildren();

        // If there is only one move left, just return that only move
        if (listOfChildren.size() == 1) {
            bestMove = listOfChildren.get(0).getMove();
        } else {
            // Loop through list of children and find the first one with the highest score
            for (int i = 0; i < listOfChildren.size(); i++) {
                if(listOfChildren.get(i).getScore() > bestScore) {
                    bestMove = listOfChildren.get(i).getMove();
                    bestScore = listOfChildren.get(i).getScore();
                }
            }
        }

        // Return move of the child with the highest score
        makeMove.execute(bestMove);
    }

    /**
     * Set make move command
     *
     * @param makeMove OnMoveCommand
     * @author Niek van der Velde
     */
    @Override
    public void setMakeMove(OnMoveCommand makeMove) {
        this.makeMove = makeMove;
    }

    @Override
    public void setNewGameState(GameModeInterface.Players[] boardState, int move, GameModeInterface.Players madeBy) {
        this.boardState = boardState;
    }

    /**
     *  Create tree with all the moves and scores
     *
     * @author Rob Kat
     * @param node
     * @param turn
     * @return
     */
    public int createTree(Node node, GameModeInterface.Players turn) {
        // Check if end of game is reached. Scores will return
        endGame(node.getBoard());
        if(!gameState.equals(GameModeInterface.FinishStates.PLAYING)) {
            switch(gameState) {
                case WON:	return 10;
                case LOST:	return -10;
                case DRAW:	return 0;
            }
        }

        // Make arraylist for all the scores of a certain level to compare later on
        ArrayList<Integer> scoreArrayList = new ArrayList();

        // Loop through all the tiles and check if their is a legal move te make and make it
        for(int i = 0; i < 9; i++) {
            GameModeInterface.Players[] values;
            values = node.getBoard();

            // If a certain value is null then there is noting placed yet. Place the enum Players with the right turn
            if(values[i] == null) {
                switch(turn){
                    case USER:
                        values[i] = GameModeInterface.Players.USER;
                        turn = GameModeInterface.Players.OPPONENT;
                        break;
                    case OPPONENT:
                        values[i] = GameModeInterface.Players.OPPONENT;
                        turn = GameModeInterface.Players.USER;
                        break;
                }

                // Create a new node and save the specific move to it
                Node child = new Node(values);
                child.setMove(i);

                // Calculate score for node created above(recursive)
                int score = createTree(child, turn);

                // Get back to current player
                switch(turn){
                    case USER:
                        turn = GameModeInterface.Players.OPPONENT;
                        break;
                    case OPPONENT:
                        turn = GameModeInterface.Players.USER;
                        break;
                }

                // Save score to node created above, add node to parent and add score to scareArrayList
                child.setScore(score);
                node.addChild(child);
                scoreArrayList.add(score);

            }
        }


        int childScore = node.getChild(0).getScore();
        int minMaxScore = arrayMinMax(scoreArrayList, turn);
        node.setScore(minMaxScore);
        int sizeScore = scoreArrayList.size();

        // Calculate score for parent
        for(int i = sizeScore - 1; i >= 0; i--) {
            if(turn == GameModeInterface.Players.USER && childScore < node.getChild(i).getScore()) {
                childScore = node.getChild(i).getScore();
            } else if(turn == GameModeInterface.Players.OPPONENT && childScore > node.getChild(i).getScore()) {
                childScore = node.getChild(i).getScore();
            }
        }

        // Return score to parent
        return childScore;
    }

    /**
     * Simple minMax function that returns either the highest int or lowest. Depending on current turn
     *
     * @author Rob Kat
     * @param array
     * @param turn
     * @return
     */
    public int arrayMinMax(ArrayList<Integer> array, GameModeInterface.Players turn){
        int score = 0;
        if(turn == GameModeInterface.Players.OPPONENT) {
            for(int i = 0; i < array.size(); i++) {
                if(array.get(i) < score) {
                    score = array.get(i);
                }
            }
        } else if(turn == GameModeInterface.Players.USER) {
            for(int i = 0; i < array.size(); i++) {
                if(array.get(i) > score) {
                    score = array.get(i);
                }
            }
        }
        return score;
    }

    /**
     * Check if end of game is reached and change the enum FinishStates if needed
     *
     * @author Rob Kat
     * @param board
     */
    public void endGame(GameModeInterface.Players[] board) {
        for(int i = 0; i < 3; i++) {

            // Horizontal or vertical WIN
            if((board[i * 3] == GameModeInterface.Players.USER &&
                board[i * 3 + 1] == GameModeInterface.Players.USER &&
                board[i * 3 + 2] == GameModeInterface.Players.USER) ||
                    (board[i] == GameModeInterface.Players.USER &&
                    board[i + 3] == GameModeInterface.Players.USER &&
                    board[i + 6] == GameModeInterface.Players.USER)) {
                gameState = GameModeInterface.FinishStates.WON;
                return;
            }
            // Horizontal or vertical LOSE
            if((board[i * 3] == GameModeInterface.Players.OPPONENT &&
                board[i * 3 + 1] == GameModeInterface.Players.OPPONENT &&
                board[i * 3 + 2] == GameModeInterface.Players.OPPONENT) ||
                    (board[i] == GameModeInterface.Players.OPPONENT &&
                    board[i + 3] == GameModeInterface.Players.OPPONENT &&
                        board[i + 6] == GameModeInterface.Players.OPPONENT)) {
                gameState = GameModeInterface.FinishStates.LOST;
                return;
            }
        }

        // Diagonal WIN
        if((board[0] == GameModeInterface.Players.USER &&
            board[4] == GameModeInterface.Players.USER &&
            board[8] == GameModeInterface.Players.USER) ||
                (board[2] == GameModeInterface.Players.USER &&
                board[4] == GameModeInterface.Players.USER &&
                board[6] == GameModeInterface.Players.USER)) {
            gameState = GameModeInterface.FinishStates.WON;
            return;
        }

        // Diagonal LOSE
        if((board[0] == GameModeInterface.Players.OPPONENT &&
            board[4] == GameModeInterface.Players.OPPONENT &&
            board[8] == GameModeInterface.Players.OPPONENT) ||
                (board[2] == GameModeInterface.Players.OPPONENT &&
                board[4] == GameModeInterface.Players.OPPONENT &&
                board[6] == GameModeInterface.Players.OPPONENT)) {
            gameState = GameModeInterface.FinishStates.LOST;
            return;
        }

        // Still playable
        for(int i = 0; i < 9; i++) {
            if(board[i] == null) {
                gameState = GameModeInterface.FinishStates.PLAYING;
                return;
            }
        }

        // DRAW
        gameState = GameModeInterface.FinishStates.DRAW;
        return;
    }

    /**
     * Set current board
     *
     * @param boardState
     */
    public void setBoard(GameModeInterface.Players[] boardState) {
        this.boardState = boardState;
        mainNode = new Node(boardState);
    }

    /**
     * On game finished
     *
     * @author Niek van der Velde
     */
    @Override
    public void gameFinished() {
        running = false;
    }
}
