package src.gameclient.gamemodes.tictactoe;

import java.util.HashMap;
import java.util.Arrays;
import java.io.IOException;

import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

import src.gameclient.gui.views.GameBoardController;
import src.gameclient.apiconnection.GameAPI;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.gamemodes.OnFinishCommand;
import src.gameclient.gamemodes.OnNextTurnCommand;
import src.gameclient.gamemodes.OnNewScoreCommand;
import src.gameclient.gamemodes.OnBoardChangeCommand;

public class TicTacToe implements GameModeInterface {
    private final int GAMESIZE = 9;
    private Players[] gameState = new Players[GAMESIZE];

    private OnFinishCommand onFinishCommand;
    private OnNextTurnCommand onNextTurnCommand;
    private OnNewScoreCommand onNewScoreCommand;
    private OnBoardChangeCommand onBoardChangeCommand;

    private Players currentTurn;
    private String opponentName;

    private GameBoardController board;
    private BorderPane boardPane;

    /**
     * TicTacToe constructor
     *
     * @author Niek van der Velde
     */
    public TicTacToe() {
        setupBoard();
    }

    /**
     * Setup tic tac toe board
     *
     * @author Niek van der Velde
     */
    private void setupBoard() {
        // Load board controller view
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(new GameBoardController().getView());
        try {
            boardPane = loader.load();
        } catch (IOException e) {

        }

        // Get controller
        board = loader.<GameBoardController>getController();
    }

    /**
     * Create board
     *
     * @author Niek van der Velde
     */
    @Override
    public void createBoard() {
        board.init();
        board.createBoard(3, 3, "white", "black");
    }

    /**
     * Set player to move
     *
     * @param player Player to move
     * @author Niek van der Velde
     */
    @Override
    public void setPlayerToMove(Players player) {
        currentTurn = player;
    }

    /**
     * Get tic tac toe playing board pane
     *
     * @return BorderPane
     * @author Niek van der Velde
     */
    @Override
    public BorderPane getBoardPane() {
        return boardPane;
    }

    /**
     * Get tic tac toe playing board controller
     *
     * @return GameBoardController
     * @author Niek van der Velde
     */
    @Override
    public GameBoardController getBoardController() {
        return board;
    }

    /**
     * Player make move
     *
     * @param tileLocation Title location of move
     * @param player Player that makes the move
     * @return Boolean
     * @author Niek van der Velde
     */
    public synchronized Boolean makeMove(int tileLocation, Players player) {
        // Check if tile is null
        if (gameState[tileLocation] == null) {
            gameState[tileLocation] = player;

            // Check if the move needs to be send to the server
            if (player == Players.USER) {
                GameAPI.getInstance().move(tileLocation);

                // Set opponents turn
                onNextTurnCommand.execute(Players.OPPONENT);
            }

            // Update game board with changes
            onBoardChangeCommand.execute(Arrays.asList(tileLocation), player);

            // Move is valid
            return true;
        }
        // Current move is not valid
        return false;
    }

    /**
     * On next turn lambda setter
     *
     * @param command OnNextTurnCommand
     * @author Niek van der Velde
     */
    @Override
    public void onNextTurn(OnNextTurnCommand command) {
        onNextTurnCommand = command;
    }

    /**
     * On new score lambda setter
     *
     * @param command OnNewScoreCommand
     * @author Niek van der Velde
     */
    @Override
    public void onNewScore (OnNewScoreCommand command) {
        onNewScoreCommand = command;
    }

    /**
     * On finish lambda setter
     *
     * @param command OnFinishCommand
     * @author Niek van der Velde
     */
    @Override
    public void onFinish(OnFinishCommand command) {
        onFinishCommand = command;
    }

    /**
     * On board change lambda setter
     *
     * @param command OnBoardChangeCommand
     * @author Niek van der Velde
     */
    @Override
    public void onBoardChange(OnBoardChangeCommand command) {
        onBoardChangeCommand = command;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    /**
     * Get has score for this gamemode
     *
     * @return Boolean
     * @author Niek van der Velde
     */
    public Boolean hasScore() {
        return false;
    }

    /**
     * Get current game state
     *
     * @return Players[]
     * @author Niek van der Velde
     */
    public Players[] getGameState() {
        return gameState;
    }
}
