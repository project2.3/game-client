package src.gameclient.gamemodes.tictactoe;

import java.util.Random;

import src.gameclient.gamemodes.GameAIInterface;
import src.gameclient.gamemodes.GameModeInterface;
import src.gameclient.logger.Logger;
import src.gameclient.gamemodes.OnMoveCommand;

public class RandomAI implements GameAIInterface {
    private GameModeInterface.Players[] gameState;
    private Logger logger = new Logger("AI");
    private OnMoveCommand makeMove;
    private Boolean running = true;

    /**
     * On AI run
     *
     * @author Niek van der Velde
     */
    public void run() {
        logger.info("Calculating next move");
        // Check how many empty tiles there are
        int emptyTiles = 0;
        int[] linkedTiles = new int[gameState.length];
        for (int index = 0; index < gameState.length; index++) {
            // Check if tile is empty
            if (gameState[index] == null) {
                logger.info("Add tile: "+ index);
                linkedTiles[emptyTiles] = index;
                emptyTiles++;
            }
        }
        logger.info("Amount of empty tiles: "+ emptyTiles);

        // Select random tile
        int selectedEmptyTile = new Random().nextInt(emptyTiles);
        logger.info("Selected empty tile: "+ selectedEmptyTile);
        int selectedTile = linkedTiles[selectedEmptyTile];
        logger.info("Selected tile: "+ selectedTile);

        makeMove.execute(selectedTile);
    }

    /**
     * Set make move command
     *
     * @param makeMove OnMoveCommand
     * @author Niek van der Velde
     */
    @Override
    public void setMakeMove(OnMoveCommand makeMove) {
        this.makeMove = makeMove;
    }

    /**
     * Set new game state
     *
     * @param gameState Current game state
     * @author Niek van der Velde
     */
    @Override
    public void setNewGameState(GameModeInterface.Players[] gameState, int move, GameModeInterface.Players madeBy) {
        logger.info("New game state");
        this.gameState = gameState;
    }

    /**
     * On game finished
     *
     * @author Niek van der Velde
     */
    @Override
    public void gameFinished() {
        running = false;
    }
}
