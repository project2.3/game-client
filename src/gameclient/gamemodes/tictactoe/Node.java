package src.gameclient.gamemodes.tictactoe;

import src.gameclient.gamemodes.GameModeInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Nodes needed for creating a tree structure
 */
public class Node
{
    private List<Node> children;
    private GameModeInterface.Players[] values;
    private int move;
    private int score;

    public Node(GameModeInterface.Players[] values)
    {
        this.children = new ArrayList<>();
        this.values = values;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public int getMove() {
        return move;
    }

    public void addChild(Node child) {
        children.add(child);
    }

    public List<Node> getChildren() {
        return children;
    }

    public Node getChild(int i) {
        return children.get(i);
    }

    public GameModeInterface.Players[] getBoard() {
        return values.clone();
    }
}
