package src.gameclient.gamemodes;

public interface OnFinishCommand {
    public void execute(GameModeInterface.FinishStates result , String reason);
}
