package src.gameclient.gamemodes;

public interface OnNewScoreCommand {
    public void execute(int userScore, int opponentScore);
}
