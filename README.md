# Game client
This game client will have support for Tic-Tac-Toe and Reversi.


## Git terminal usage 
**Do not commit and push on the master branch!**
### Commit your changes 
* Add your changes to the commit: `git add .`
* Commit your changes: `git commit -m "[Commitmessage]"`
* Push your changes: `git push origin [branchname]`

### Checkout to new branch 
* Fetch the new branch: `git fetch`
* Checkout to the branch: `git checkout [branchname]`

### Get changes of branch
* Pull the new change: `git pull origin [branchname]`

### Get master branch on your own branch 
* Go back to master: `git checkout master`
* Pull master changes: `git pull origin master`
* Go back to your branch: `git checkout [branchname]`
* Merge master branch into your branch: `git merge master`
* Push the merge to your branch: `git push origin [branchname]`